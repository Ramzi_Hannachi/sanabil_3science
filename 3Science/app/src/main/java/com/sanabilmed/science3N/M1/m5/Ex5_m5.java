package com.sanabilmed.science3N.M1.m5;

import java.util.Random;
import java.util.Vector;

import android.app.Activity;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Message;
import android.os.PowerManager.WakeLock;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sanabilmed.science3N.Act_Modules;
import com.sanabilmed.science3N.R;
import com.sanabilmed.science3N.M1.m1.Image;

public class Ex5_m5 extends DragWithFleche_Ex5_m5
{
	ImageView obj_1,obj_2,obj_3,obj_4;
	LinearLayout za1, za2, za3, za4, za5,za6,za7,za8,za9,za10;
	LinearLayout zd1,zd2,zd3,zd4,zd5,zd6;
	private WakeLock mWakeLock;
	private ImageView rep, ex2, ex1, ex4, ex3, ex6,audio,
	imgVer, imgCorr, img2,exit, home;

	private MediaPlayer mp;
	private AnimationSet animation;
	private boolean scalingComplete = false;
	Activity act;
	ClipData data;
	int randomInt;

	private static final long active = 2000;
	private static final int STOP = 0;
	private Message msg;

	public void onCreate(Bundle savedInstanceState) 
	{
		setContentView(R.layout.ex5_m5);

		exit = (ImageView) findViewById(R.id.exit);
		home = (ImageView) findViewById(R.id.home);
		img2 = (ImageView) findViewById(R.id.b2);

		ex1 = (ImageView) findViewById(R.id.ex_1);
		ex2 = (ImageView) findViewById(R.id.ex_2);
		ex3 = (ImageView) findViewById(R.id.ex_3);
		ex4 = (ImageView) findViewById(R.id.ex_4);
		ex6 = (ImageView) findViewById(R.id.ex_6);

		imgCorr = (ImageView) findViewById(R.id.imgCorr);
		imgVer = (ImageView) findViewById(R.id.imgVer);
		rep = (ImageView) findViewById(R.id.repeat);
		audio = (ImageView) findViewById(R.id.jouerSon);

		act = this;
		//
		content_line = (RelativeLayout) findViewById(R.id.content_line);

		obj_1 = (ImageView) findViewById(R.id.depart1);
		obj_2 = (ImageView) findViewById(R.id.depart2);
		obj_3 = (ImageView) findViewById(R.id.depart3);
		obj_4 = (ImageView) findViewById(R.id.depart4);

		images = new Vector<View>();
		images.add(obj_1);
		images.add(obj_2);
		images.add(obj_3);
		images.add(obj_4);

		rep_images = new Vector<View>();
		rep_images.add(obj_1);
		rep_images.add(obj_2);
		rep_images.add(obj_3);
		rep_images.add(obj_4);

		image_id = new Vector<Image>();
		image_id.add(new Image(R.drawable.e3_m5_ex5_c_select, R.drawable.e3_m5_ex5_c_vrai,R.drawable.e3_m5_ex5_c_faux));
		// LinearLayout de depart qui contenent l'image :ramzi
		zd1 = (LinearLayout) findViewById(R.id.zd1);
		zd2 = (LinearLayout) findViewById(R.id.zd2);
		zd3 = (LinearLayout) findViewById(R.id.zd3);
		zd4 = (LinearLayout) findViewById(R.id.zd4);

		zone_depart = new Vector<ViewGroup>();
		zone_depart.add(zd1);
		zone_depart.add(zd2);
		zone_depart.add(zd3);
		zone_depart.add(zd4);
		// LinearLayout d'arriver pour déposer l'image :ramzi
		za1 = (LinearLayout) findViewById(R.id.za1);
		za2 = (LinearLayout) findViewById(R.id.za2);
		za3 = (LinearLayout) findViewById(R.id.za3);
		za4 = (LinearLayout) findViewById(R.id.za4);
		za5 = (LinearLayout) findViewById(R.id.za5);
		za6 = (LinearLayout) findViewById(R.id.za6);
		za7 = (LinearLayout) findViewById(R.id.za7);

		zone_arrivee = new Vector<ViewGroup>();
		zone_arrivee.add(za1);
		zone_arrivee.add(za2);
		zone_arrivee.add(za3);
		zone_arrivee.add(za4);
		zone_arrivee.add(za5);
		zone_arrivee.add(za6);
		zone_arrivee.add(za7);

		// ////////////////////////////fin gestion
		// mp////////////////////////////////

		Animation fadeIn = new AlphaAnimation(0, 1);
		fadeIn.setInterpolator(new DecelerateInterpolator()); // add this
		fadeIn.setDuration(1000);

		Animation fadeOut = new AlphaAnimation(1, 0);
		fadeOut.setInterpolator(new AccelerateInterpolator()); // and this
		fadeOut.setStartOffset(2500);// 3500
		fadeOut.setDuration(1000);

		animation = new AnimationSet(true); // change to false
		animation.addAnimation(fadeIn);
		animation.addAnimation(fadeOut);

		super.onCreate(savedInstanceState);

		imgVer.setOnClickListener(new OnClickListener()
		{
			public void onClick(View v) 
			{  
				Random rand = new Random();
				randomInt = rand.nextInt();
				imgCorr.setBackgroundDrawable(null);
				imgCorr.setBackgroundDrawable(null);
				imgCorr.setBackgroundDrawable(null);
				try_stop_mp(mp);
				switch (verifierReponse()) 
				{
				case 0:
					imgCorr.setVisibility(1);
					imgCorr.setBackgroundResource(R.drawable.mascotte_incomplet);
					imgCorr.startAnimation(animation);
					imgCorr.setVisibility(View.INVISIBLE);
					mp = MediaPlayer.create(getApplicationContext(),R.raw.mascotte_incomplet);
					mp.start();
					break;

				case 1:
					imgCorr.setVisibility(1);
					imgCorr.setBackgroundResource(R.drawable.mascotte_faux_2);
					imgCorr.startAnimation(animation);
					imgCorr.setVisibility(View.INVISIBLE);
					mp = MediaPlayer.create(getApplicationContext(),R.raw.mascotte_faux_2);
					mp.start();
					break;

				case 2:
					imgCorr.setVisibility(1);
					imgCorr.setBackgroundResource(R.drawable.mascotte_vrai_1);
					imgCorr.startAnimation(animation);
					imgCorr.setVisibility(View.INVISIBLE);
					mp = MediaPlayer.create(getApplicationContext(),R.raw.mascotte_vrai_1);
					mp.start();
					break;
				}
			}
		});

		imgCorr.setOnClickListener(new OnClickListener() 
		{
			public void onClick(View v)
			{
				imgCorr.setVisibility(View.INVISIBLE);
			}
		});
		rep.setOnClickListener(new OnClickListener() 
		{
			public void onClick(View v) 
			{
				try_stop_mp(mp);
				finish();
				startActivity(getIntent());
			}
		});
		audio.setOnClickListener(new OnClickListener()
		{
			public void onClick(View v) 
			{
				try_stop_mp(mp);
				mp = new MediaPlayer();
				mp = MediaPlayer.create(Ex5_m5.this,R.raw.e3_m5_ex5);
				mp.start();
			}
		});


		exit.setOnClickListener(new OnClickListener()
		{
			public void onClick(View v)
			{
				try_stop_mp(mp);
				finish();

			}
		});
		home.setOnClickListener(new OnClickListener() 
		{
			public void onClick(View v)
			{
				try_stop_mp(mp);
				startActivity(new Intent(getApplicationContext(), Act_Modules.class));
				act.finish();
			}
		});
		img2.setOnClickListener(new OnClickListener()
		{
			public void onClick(View v) 
			{
				try_stop_mp(mp);
				Intent i2 = new Intent(getApplicationContext(),Page1_m5.class);
				startActivity(i2);
				act.finish();
			}
		});
		ex1.setOnClickListener(new OnClickListener()
		{
			public void onClick(View v)
			{
				try_stop_mp(mp);
				startActivity(new Intent(getApplicationContext(),Ex1_m5.class));
				act.finish();
			}
		});
		ex2.setOnClickListener(new OnClickListener()
		{
			public void onClick(View v) 
			{
				try_stop_mp(mp);
				startActivity(new Intent(getApplicationContext(),Ex2_m5.class));
				act.finish();
			}
		});
		ex4.setOnClickListener(new OnClickListener() 
		{
			public void onClick(View v) 
			{
				try_stop_mp(mp);
				startActivity(new Intent(getApplicationContext(),Ex4_m5.class));
				act.finish();
			}
		});
		ex3.setOnClickListener(new OnClickListener()
		{
			public void onClick(View v) 
			{
				try_stop_mp(mp);
				startActivity(new Intent(getApplicationContext(),Ex3_m5.class));
				act.finish();
			}
		});
		ex6.setOnClickListener(new OnClickListener()
		{
			public void onClick(View v)
			{
				try_stop_mp(mp);
				startActivity(new Intent(getApplicationContext(),Ex6_m5.class));
				act.finish();
			}
		});


	}







	public void try_stop_mp(MediaPlayer mp)
	{
		try {

			if (mp != null) 
			{
				mp.stop();
				mp.release();
			}

		} catch (Exception e)
		{
			// TODO: handle exception
		}
	}









	// ///////////////////////////////////////////////////////////////////////////////////////////
	public void onWindowFocusChanged(boolean hasFocus) {
		if (!scalingComplete) // only do this once
		{
			scaleContents(findViewById(R.id.contents),
					findViewById(R.id.container));
			scalingComplete = true;
		}
		super.onWindowFocusChanged(hasFocus);
	}

	/**
	 * Called when the views have been created. We override this in order to
	 * scale the UI, which we can't do before this.
	 */
	@Override
	public View onCreateView(String name, Context context, AttributeSet attrs) {
		View view = super.onCreateView(name, context, attrs);
		return view;
	}

	private void scaleContents(View rootView, View container) {
		float xScale = (float) container.getWidth() / rootView.getWidth();
		float yScale = (float) container.getHeight() / rootView.getHeight();
		float scale = Math.min(xScale, yScale);
		scaleViewAndChildren(rootView, scale);
	}

	public static void scaleViewAndChildren(View root, float scale) {
		ViewGroup.LayoutParams layoutParams = root.getLayoutParams();

		// Scale the view itself
		if (layoutParams.width != ViewGroup.LayoutParams.FILL_PARENT
				&& layoutParams.width != ViewGroup.LayoutParams.WRAP_CONTENT) {
			layoutParams.width *= scale;
		}
		if (layoutParams.height != ViewGroup.LayoutParams.FILL_PARENT
				&& layoutParams.height != ViewGroup.LayoutParams.WRAP_CONTENT) {
			layoutParams.height *= scale;
		}

		if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
			ViewGroup.MarginLayoutParams marginParams = (ViewGroup.MarginLayoutParams) layoutParams;
			marginParams.leftMargin *= scale;
			marginParams.rightMargin *= scale;
			marginParams.topMargin *= scale;
			marginParams.bottomMargin *= scale;
		}

		root.setLayoutParams(layoutParams);

		root.setPadding((int) (root.getPaddingLeft() * scale),
				(int) (root.getPaddingTop() * scale),
				(int) (root.getPaddingRight() * scale),
				(int) (root.getPaddingBottom() * scale));

		if (root instanceof TextView) {
			TextView textView = (TextView) root;
			textView.setTextSize(textView.getTextSize() * scale);
		}

		if (root instanceof ViewGroup) {
			ViewGroup groupView = (ViewGroup) root;
			for (int cnt = 0; cnt < groupView.getChildCount(); ++cnt)
				scaleViewAndChildren(groupView.getChildAt(cnt), scale);
		}
	}



	public boolean isOnline() {

		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}


}