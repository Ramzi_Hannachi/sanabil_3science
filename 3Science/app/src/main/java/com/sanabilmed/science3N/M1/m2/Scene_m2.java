package com.sanabilmed.science3N.M1.m2;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.sanabilmed.science3N.R;
import com.sanabilmed.science3N.M1.m1.Ex1_m1;
import com.sanabilmed.science3N.M1.m1.Module1_m1;
import com.sanabilmed.science3N.M1.m2.NavigationBarreView_M2.OnDispatchClickListener;
import com.sanabilmed.science3N.M1.m2.NavigationBarreView_M2.OnItemClickListener;
import com.sanabilmed.science3N.otherClasses.Hh;
import com.sanabilmed.science3N.otherClasses.ScaleManager;

public class Scene_m2 extends Activity implements OnClickListener,
		OnDispatchClickListener, OnItemClickListener {
	private MediaPlayer mp, bg_mp;
	private ImageView imNuage, imButScroll, imBg, exit, home;
	private NavigationBarreView_M2 mActionBar;
	private int X_SCALE, Y_SCALE;
	private float MARGE;
	private Handler handler;
	private WakeLock mWakeLock;
	private ImageView img1,img2,img3,img4;
	protected boolean scalingComplete = false;
	Activity act;

	// TO herit

	protected int imageDrawable, sound, bg_sound;
	boolean hasNext, zoomed;
	protected Class<?> nextActivity;

	@SuppressWarnings("rawtypes")
	Class[] scenes = { Page1_m2.class,Page2_m2.class};

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.scene_m2);

		Hh.StopedThread = false;
		mp = new MediaPlayer();
		bg_mp = new MediaPlayer();

		
		// keep screen on
				PowerManager power = (PowerManager) getSystemService(Context.POWER_SERVICE);
				mWakeLock = power.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK,
						"My Tag");
				mWakeLock.acquire();
				getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
				
		imNuage = (ImageView) findViewById(R.id.imNuage);
		imNuage.setOnClickListener(this);
		imNuage.setVisibility(View.INVISIBLE);
		imButScroll = (ImageView) findViewById(R.id.imButScrolle);
		imButScroll.setOnClickListener(this);
		imBg = (ImageView) findViewById(R.id.image_scene);

		imBg.setImageResource(imageDrawable);

		startZoomOut(imBg);
		mActionBar = (NavigationBarreView_M2) findViewById(R.id.nav_barre);
		mActionBar.setOnDispatchClickListener(this);
		mActionBar.setOnDispatchItemClickListener(this);

		act = this;

		img1 = (ImageView) findViewById(R.id.b1);
		img1.setOnClickListener(this);
		exit = (ImageView) findViewById(R.id.exit);
		home = (ImageView) findViewById(R.id.home);


		img1.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				try_stop_mp(mp);
				Intent i2 = new Intent(getApplicationContext(),
						Ex1_m2.class);
				startActivity(i2);
				act.finish();
			}
		});
		
		
		

		exit.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				try_stop_mp(mp);
				try_stop_mp(bg_mp);
				act.finish();
			}
		});

		home.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Hh.selectedPage = 0;
				try_stop_mp(mp);
				try_stop_mp(bg_mp);
				Intent i2 = new Intent(getApplicationContext(),
						Module1_m2.class);
				startActivity(i2);
				finish();
			}
		});

		mp.setOnCompletionListener(new OnCompletionListener() {
			public void onCompletion(MediaPlayer mp) {
				if (hasNext) {
					try_stop_mp(mp);
					Intent i2 = new Intent(getApplicationContext(),
							nextActivity);
					startActivity(i2);
					act.finish();
				}
			}
		});
	}
	
	protected void onDestroy() {
		mp.release();
		bg_mp.release();
		mWakeLock.release();
		super.onDestroy();
	}

	protected void onRestart() {
		super.onRestart();
	}

	protected void onPause() {
		act.finish();
		super.onPause();
	}

	public void ShowNextButton() {
		Animation myFadeInAnimation;

		myFadeInAnimation = AnimationUtils.loadAnimation(this, R.anim.fadein);
		imNuage.startAnimation(myFadeInAnimation);
		AnimationDrawable frameAnimation;
		frameAnimation = (AnimationDrawable) imNuage.getBackground();
		frameAnimation.start();
		imNuage.setVisibility(View.VISIBLE);
	}

	public void onClick(View arg0) {
		switch (arg0.getId()) {

		case R.id.imNuage:
			Hh.selectedPage = 0;
			Intent intent = new Intent(this, Scene_m2.class);
			startActivityForResult(intent, 1000);
			overridePendingTransition(android.R.anim.fade_in,
					android.R.anim.fade_out);
			try_stop_mp(mp);
			try_stop_mp(bg_mp);
			finish();
			Hh.StopedThread = true;
			break;
		case R.id.imButScrolle:
			NavigationBarreView_M2.showBarre();
			break;
		default:
			break;
		}

	}

	public void onDispatchClick(int id) {
		switch (id) {
		case R.id.imNext:
			Hh.selectedPage = 0;
			NavigationBarreView_M2.MoveToNextItem();
			break;
		case R.id.imPrev:
			Hh.selectedPage = 0;
			NavigationBarreView_M2.MoveToPrevItem();
			break;
		case R.id.imHome:
			Hh.selectedPage = 0;
			Intent i2 = new Intent(getApplicationContext(),
					Module1_m1.class);
			startActivity(i2);
			finish();
			try_stop_mp(mp);
			try_stop_mp(bg_mp);
			Hh.StopedThread = true;
			break;
		}
	}

	public void startZoomOut(final ImageView image) {
		
		if (sound != 0) {
			mp = MediaPlayer.create(getApplicationContext(), sound);
			mp.start();
		}
		if (bg_sound != 0) {
			bg_mp = MediaPlayer.create(getApplicationContext(), bg_sound);
			bg_mp.start();
		}
		
	}

	public void onDispatchItemClick(int position) {
		Intent intent = null;
		Hh.selectedPage = position;
		intent = new Intent(this, scenes[position]);
		startActivityForResult(intent, 1000);
		overridePendingTransition(android.R.anim.fade_in,
				android.R.anim.fade_out);
		try_stop_mp(mp);
		try_stop_mp(bg_mp);
		finish();
	}
	
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		if (!scalingComplete) {
			ScaleManager sm = new ScaleManager();
			sm.scaleContents(findViewById(R.id.contents),
					findViewById(R.id.container));
			scalingComplete = true;
		}
		super.onWindowFocusChanged(hasFocus);
	}

	public void try_stop_mp(MediaPlayer mp) {
		if (mp != null) {
			mp.stop();
			mp.release();
		}
	}
}
