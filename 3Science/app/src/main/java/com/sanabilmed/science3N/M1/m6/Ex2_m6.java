package com.sanabilmed.science3N.M1.m6;

import java.util.Random;

import android.app.Activity;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable.ConstantState;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.sanabilmed.science3N.Act_Modules;
import com.sanabilmed.science3N.R;


public class Ex2_m6 extends Activity 
{
	// image 1
	private ImageView _image1_prop_vrais;
	private ImageView _image1_prop_faux;
	private ImageView _image1_repo;
	// image 2
	private ImageView _image2_prop_vrais;
	private ImageView _image2_prop_faux;
	private ImageView _image2_repo;
	// image 3
	private ImageView _image3_prop_vrais;
	private ImageView _image3_prop_faux;
	private ImageView _image3_repo;

	//private WakeLock mWakeLock;
	private ImageView rep, ex3 , ex1, ex6, ex4, ex5, ex7,audio,
	imgVer, imgCorr, img2, exit, home;
	private MediaPlayer mp;

	AnimationSet animation;
	private boolean scalingComplete = false;
	Activity act;
	ClipData data;
	int randomInt;

	private static final long active = 2000;
	private static final int STOP = 0;
	private Message msg;

	int test = 0;

	public void onCreate(Bundle savedInstanceState)
	{
		setContentView(R.layout.ex2_m6);
		exit = (ImageView) findViewById(R.id.exit);
		home = (ImageView) findViewById(R.id.home);

		act = this;

		img2 = (ImageView) findViewById(R.id.b2);

		ex1 = (ImageView) findViewById(R.id.ex_1);
		ex3 = (ImageView) findViewById(R.id.ex_3);
		ex4 = (ImageView) findViewById(R.id.ex_4);
		ex5 = (ImageView) findViewById(R.id.ex_5);
		ex6 = (ImageView) findViewById(R.id.ex_6);
		ex7=(ImageView) findViewById(R.id.ex_7);

		imgCorr = (ImageView) findViewById(R.id.imgCorr);
		imgVer = (ImageView) findViewById(R.id.imgVer);
		rep = (ImageView) findViewById(R.id.repeat);
		audio = (ImageView) findViewById(R.id.jouerSon);
		// Ex2 ♣

		_image1_prop_vrais=(ImageView) findViewById(R.id.image1_prop_vrais);
		_image1_prop_faux=(ImageView) findViewById(R.id.image1_prop_faux);
		_image1_repo=(ImageView) findViewById(R.id.image1_repo);
		// image 2
		_image2_prop_vrais=(ImageView) findViewById(R.id.image2_prop_vrais);
		_image2_prop_faux=(ImageView) findViewById(R.id.image2_prop_faux);
		_image2_repo=(ImageView) findViewById(R.id.image2_repo);
		// image 3
		_image3_prop_vrais=(ImageView) findViewById(R.id.image3_prop_vrais);
		_image3_prop_faux=(ImageView) findViewById(R.id.image3_prop_faux);
		_image3_repo=(ImageView) findViewById(R.id.image3_repo);
		// Animaiton ♣
		Animation fadeIn = new AlphaAnimation(0, 1);
		fadeIn.setInterpolator(new DecelerateInterpolator()); // add this
		fadeIn.setDuration(1000);

		Animation fadeOut = new AlphaAnimation(1, 0);
		fadeOut.setInterpolator(new AccelerateInterpolator()); // and this
		fadeOut.setStartOffset(2500);// 3500
		fadeOut.setDuration(1000);

		animation = new AnimationSet(true); // change to false
		animation.addAnimation(fadeIn);
		animation.addAnimation(fadeOut);

		super.onCreate(savedInstanceState);
		// image 1
		_image1_prop_vrais.setOnClickListener(new OnClickListener() 
		{
			public void onClick(View v) 
			{
				_image1_repo.setImageDrawable(getResources().getDrawable(R.drawable.e3_m6_ex1a_ic1_select));
			}
		});
		_image1_prop_faux.setOnClickListener(new OnClickListener() 
		{
			public void onClick(View v) 
			{
				_image1_repo.setImageDrawable(getResources().getDrawable(R.drawable.e3_m6_ex1a_ic2_select));
			}
		});
		// image2
		_image2_prop_vrais.setOnClickListener(new OnClickListener() 
		{
			public void onClick(View v) 
			{
				_image2_repo.setImageDrawable(getResources().getDrawable(R.drawable.e3_m6_ex1a_ic1_select));
			}
		});
		_image2_prop_faux.setOnClickListener(new OnClickListener() 
		{
			public void onClick(View v) 
			{
				_image2_repo.setImageDrawable(getResources().getDrawable(R.drawable.e3_m6_ex1a_ic2_select));
			}
		});
		// image3
		_image3_prop_vrais.setOnClickListener(new OnClickListener() 
		{
			public void onClick(View v) 
			{
				_image3_repo.setImageDrawable(getResources().getDrawable(R.drawable.e3_m6_ex1a_ic1_select));
			}
		});
		_image3_prop_faux.setOnClickListener(new OnClickListener() 
		{
			public void onClick(View v) 
			{
				_image3_repo.setImageDrawable(getResources().getDrawable(R.drawable.e3_m6_ex1a_ic2_select));
			}
		});
		//////
		imgVer.setOnClickListener(new OnClickListener()
		{
			public void onClick(View v)
			{
				Random rand = new Random();
				randomInt = rand.nextInt();
				imgCorr.setBackgroundDrawable(null);
				imgCorr.setBackgroundDrawable(null);
				imgCorr.setBackgroundDrawable(null);
				try_stop_mp(mp);
				int test=Verifier_Reponse();

				if (test==1) 
				{
					imgCorr.setVisibility(1);
					imgCorr.setBackgroundResource(R.drawable.mascotte_incomplet);
					imgCorr.startAnimation(animation);
					imgCorr.setVisibility(View.INVISIBLE);
					mp = MediaPlayer.create(getApplicationContext(),R.raw.mascotte_incomplet);
					mp.start();
				}
				if (test==2) 
				{
					imgCorr.setVisibility(1);
					imgCorr.setBackgroundResource(R.drawable.mascotte_vrai_1);
					imgCorr.startAnimation(animation);
					imgCorr.setVisibility(View.INVISIBLE);
					mp = MediaPlayer.create(getApplicationContext(),
							R.raw.mascotte_vrai_1);
					mp.start();

				}
				if (test==0) 
				{
					imgCorr.setVisibility(1);
					imgCorr.setBackgroundResource(R.drawable.mascotte_faux_2);
					imgCorr.startAnimation(animation);
					imgCorr.setVisibility(View.INVISIBLE);

					mp = MediaPlayer.create(getApplicationContext(),R.raw.mascotte_faux_2);
					mp.start();

					Message msg = new Message();
					msg.what = STOP;
					Handler.sendMessageDelayed(msg, active);
				}


			}
		});

		img2.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				//				try_stop_mp(mp);
				//				Intent i2 = new Intent(getApplicationContext(), Page1_m5.class);
				//				startActivity(i2);
				//				act.finish();
			}
		});

		home.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				try_stop_mp(mp);
				startActivity(new Intent(getApplicationContext(), Act_Modules.class));

				act.finish();

			}
		});
		exit.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				try_stop_mp(mp);
				finish();

			}
		});
		ex1.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				try_stop_mp(mp);
				startActivity(new Intent(getApplicationContext(), Ex1_m6.class));

				act.finish();

			}
		});

		ex6.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				try_stop_mp(mp);
				startActivity(new Intent(getApplicationContext(), Ex6_m6.class));

				act.finish();

			}
		});

		ex3.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				try_stop_mp(mp);
				startActivity(new Intent(getApplicationContext(), Ex3_m6.class));

				act.finish();

			}
		});
		ex4.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {

				try_stop_mp(mp);
				startActivity(new Intent(getApplicationContext(), Ex4_m6.class));

				act.finish();
			}
		});


		ex5.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {

				try_stop_mp(mp);
				startActivity(new Intent(getApplicationContext(), Ex5_m6.class));

				act.finish();
			}
		});
		ex7.setOnClickListener(new OnClickListener() 
		{
			public void onClick(View v)
			{
				try_stop_mp(mp);
				startActivity(new Intent(getApplicationContext(), Ex7_m6.class));

				act.finish();
			}
		});



		imgCorr.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				imgCorr.setVisibility(View.INVISIBLE);

			}
		});

		rep.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				try_stop_mp(mp);
				finish();
				startActivity(getIntent());
			}
		});

		audio.setOnClickListener(new OnClickListener() 
		{
			public void onClick(View v)
			{
				try_stop_mp(mp);
				mp = new MediaPlayer();
				mp = MediaPlayer.create(Ex2_m6.this, R.raw.e3_m6_ex1);
				mp.start();
			}
		});

	}


	public int Verifier_Reponse()
	{
		int test=0;
		ConstantState image1_repo=_image1_repo.getDrawable().getConstantState();
		ConstantState image2_repo=_image2_repo.getDrawable().getConstantState();
		ConstantState image3_repo=_image3_repo.getDrawable().getConstantState();
		// initial
		ConstantState image_vrais_initial=getResources().getDrawable(R.drawable.e3_m6_ex1a_ic1_select).getConstantState();
		ConstantState image_faux_initial=getResources().getDrawable(R.drawable.e3_m6_ex1a_ic2_select).getConstantState();
		ConstantState image_initial=getResources().getDrawable(R.drawable.e3_m6_ex1a_ic2_intial).getConstantState();
		// vert
		ConstantState image_vrais_vert=getResources().getDrawable(R.drawable.e3_m6_ex1a_ic1_vrai).getConstantState();
		ConstantState image_faux_vert=getResources().getDrawable(R.drawable.e3_m6_ex1a_ic2_vrai).getConstantState();

		if (image1_repo.equals(image_initial) || image2_repo.equals(image_initial) || image3_repo.equals(image_initial)) 
		{ 
			return 1;
		}else
		{
			// 1
			if (image1_repo.equals(image_faux_initial) || image1_repo.equals(image_faux_vert) )
			{ 
				test++;
				_image1_repo.setImageDrawable(getResources().getDrawable(R.drawable.e3_m6_ex1a_ic2_vrai));
			}else
			{
				_image1_repo.setImageDrawable(getResources().getDrawable(R.drawable.e3_m6_ex1a_ic1_faux));
			}
			// 2
			if (image2_repo.equals(image_vrais_initial) || image2_repo.equals(image_vrais_vert))
			{
				test++;
				_image2_repo.setImageDrawable(getResources().getDrawable(R.drawable.e3_m6_ex1a_ic1_vrai));
			}else
			{
				_image2_repo.setImageDrawable(getResources().getDrawable(R.drawable.e3_m6_ex1a_ic2_faux));
			}
			// 3
			if (image3_repo.equals(image_faux_initial) || image3_repo.equals(image_faux_vert))
			{
				test++;
				_image3_repo.setImageDrawable(getResources().getDrawable(R.drawable.e3_m6_ex1a_ic2_vrai));
			}else
			{
				_image3_repo.setImageDrawable(getResources().getDrawable(R.drawable.e3_m6_ex1a_ic1_faux));
			}

			if (test==3)
			{
				return 2;
			}else 
			{
				return 0;
			}
		}
	}




	private Handler Handler = new Handler() 
	{
		public void handleMessage(Message msg) 
		{
			super.handleMessage(msg);
		}

	};





	public void try_stop_mp(MediaPlayer mp) 
	{
		try {
			if (mp != null) 
			{
				mp.stop();
				mp.release();
			}

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	//	protected void onDestroy() {
	//		mp.release();
	//		mWakeLock.release();
	//		super.onDestroy();
	//	}

	protected void onRestart() {
		super.onRestart();
	}

	protected void onPause() {
		act.finish();
		super.onPause();
	}
	public boolean isOnline() {

		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}

	// ///////////////////////////////////////////////////////////////////////////////////////////
	public void onWindowFocusChanged(boolean hasFocus) {
		if (!scalingComplete) // only do this once
		{
			scaleContents(findViewById(R.id.contents),
					findViewById(R.id.container));
			scalingComplete = true;
		}
		super.onWindowFocusChanged(hasFocus);
	}

	/**
	 * Called when the views have been created. We override this in order to
	 * scale the UI, which we can't do before this.
	 */
	@Override
	public View onCreateView(String name, Context context, AttributeSet attrs) {
		View view = super.onCreateView(name, context, attrs);
		return view;
	}

	private void scaleContents(View rootView, View container) {
		float xScale = (float) container.getWidth() / rootView.getWidth();
		float yScale = (float) container.getHeight() / rootView.getHeight();
		float scale = Math.min(xScale, yScale);
		scaleViewAndChildren(rootView, scale);
	}

	public static void scaleViewAndChildren(View root, float scale) {
		ViewGroup.LayoutParams layoutParams = root.getLayoutParams();

		// Scale the view itself
		if (layoutParams.width != ViewGroup.LayoutParams.FILL_PARENT
				&& layoutParams.width != ViewGroup.LayoutParams.WRAP_CONTENT) {
			layoutParams.width *= scale;
		}
		if (layoutParams.height != ViewGroup.LayoutParams.FILL_PARENT
				&& layoutParams.height != ViewGroup.LayoutParams.WRAP_CONTENT) {
			layoutParams.height *= scale;
		}

		if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
			ViewGroup.MarginLayoutParams marginParams = (ViewGroup.MarginLayoutParams) layoutParams;
			marginParams.leftMargin *= scale;
			marginParams.rightMargin *= scale;
			marginParams.topMargin *= scale;
			marginParams.bottomMargin *= scale;
		}

		root.setLayoutParams(layoutParams);

		root.setPadding((int) (root.getPaddingLeft() * scale),
				(int) (root.getPaddingTop() * scale),
				(int) (root.getPaddingRight() * scale),
				(int) (root.getPaddingBottom() * scale));

		if (root instanceof TextView) {
			TextView textView = (TextView) root;
			textView.setTextSize(textView.getTextSize() * scale);
		}

		if (root instanceof ViewGroup) {
			ViewGroup groupView = (ViewGroup) root;
			for (int cnt = 0; cnt < groupView.getChildCount(); ++cnt)
				scaleViewAndChildren(groupView.getChildAt(cnt), scale);
		}
	}



}