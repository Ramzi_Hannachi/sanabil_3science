package com.sanabilmed.science3N.M1.m3;

import java.util.Random;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager.WakeLock;
import android.util.AttributeSet;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.DragShadowBuilder;
import android.view.View.OnClickListener;
import android.view.View.OnDragListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sanabilmed.science3N.Act_Modules;
import com.sanabilmed.science3N.R;

public class Ex2_m3 extends Activity {

	ImageView obj_1, obj_2, obj_3, obj_4, obj_5;
	LinearLayout za1, za2, za3, za4, za5;
	LinearLayout zd1, zd2, zd3, zd4, zd5;
	private WakeLock mWakeLock;
	private ImageView rep, ex3, ex1, ex2, ex4, ex5, ex6, ex7, ex8, ex9, audio,
			imgVer, imgCorr, imgVerEx2, img1, img2, img3, img4, exit, home;
	private MediaPlayer mp;
	private ImageView rep1, rep2, rep3, rep4, rep5, rep6, rep7, rep8;

	AnimationSet animation;
	boolean _rep1 = false, _rep2 = false, _rep3 = false, _rep4 = false,
			_rep5 = false, _rep6 = false, _rep7 = false, _rep8 = false,
			_rep9 = false, _rep10 = false, _rep11 = false, _rep12 = false;
	private boolean scalingComplete = false;
	Activity act;
	ClipData data;
	int randomInt;

	private static final long active = 2000;
	private static final int STOP = 0;
	private Message msg;
	private ImageView prop_1, prop_2, prop_3, prop_4, prop_5, prop_6, prop_7,
			prop_8, prop_9, prop_10, prop_11, prop_12;

	private RelativeLayout conteneur_1_1, conteneur_1_2, conteneur_1_3,
			conteneur_1_4, conteneur_1_5, conteneur_1_6;
	private RelativeLayout conteneur_2_1, conteneur_2_2, conteneur_2_3,
			conteneur_2_4, conteneur_2_5, conteneur_2_6;
	private RelativeLayout conteneur_3_1, conteneur_3_2, conteneur_3_3,
			conteneur_3_4, conteneur_3_5, conteneur_3_6;

	private Boolean _conteneur_1_1 = false, _conteneur_1_2 = false,
			_conteneur_1_3 = false, _conteneur_1_4 = false,
			_conteneur_1_5 = false, _conteneur_1_6 = false;
	private Boolean _conteneur_2_1 = false, _conteneur_2_2 = false,
			_conteneur_2_3 = false, _conteneur_2_4 = false,
			_conteneur_2_5 = false, _conteneur_2_6 = false;
	private Boolean _conteneur_3_1 = false, _conteneur_3_2 = false,
			_conteneur_3_3 = false, _conteneur_3_4 = false,
			_conteneur_3_5 = false, _conteneur_3_6 = false;

	private Boolean __conteneur_1_1 = false, __conteneur_1_2 = false,
			__conteneur_1_3 = false, __conteneur_1_4 = false,
			__conteneur_1_5 = false, __conteneur_1_6 = false;
	private Boolean __conteneur_2_1 = false, __conteneur_2_2 = false,
			__conteneur_2_3 = false, __conteneur_2_4 = false,
			__conteneur_2_5 = false, __conteneur_2_6 = false;
	private Boolean __conteneur_3_1 = false, __conteneur_3_2 = false,
			__conteneur_3_3 = false, __conteneur_3_4 = false,
			__conteneur_3_5 = false, __conteneur_3_6 = false;
	int test = 0;
	private RelativeLayout zd_1, zd_2, zd_3, zd_4, zd_5, zd_6, zd_7, zd_8,
			zd_9, zd_10, zd_11, zd_12;
	int cont_1 = 0, cont_2 = 0, cont_3 = 0;

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public void onCreate(Bundle savedInstanceState) {

		setContentView(R.layout.ex2_m3);
		exit = (ImageView) findViewById(R.id.exit);
		home = (ImageView) findViewById(R.id.home);

		act = this;

		img2 = (ImageView) findViewById(R.id.b2);
		ex1 = (ImageView) findViewById(R.id.ex_1);
		ex3 = (ImageView) findViewById(R.id.ex_3);
		ex2 = (ImageView) findViewById(R.id.ex_2);
		ex4 = (ImageView) findViewById(R.id.ex_4);
		ex5 = (ImageView) findViewById(R.id.ex_5);
		ex6 = (ImageView) findViewById(R.id.ex_6);
		ex7 = (ImageView) findViewById(R.id.ex_7);
		ex8 = (ImageView) findViewById(R.id.ex_8);
		ex9 = (ImageView) findViewById(R.id.ex_9);
		imgCorr = (ImageView) findViewById(R.id.imgCorr);
		imgVer = (ImageView) findViewById(R.id.imgVer);
		rep = (ImageView) findViewById(R.id.repeat);
		audio = (ImageView) findViewById(R.id.jouerSon);

		// ////////////////////////////fin gestion
		// mp////////////////////////////////

		prop_1 = (ImageView) findViewById(R.id.obj_1);
		prop_2 = (ImageView) findViewById(R.id.obj_2);
		prop_3 = (ImageView) findViewById(R.id.obj_3);
		prop_4 = (ImageView) findViewById(R.id.obj_4);
		prop_5 = (ImageView) findViewById(R.id.obj_5);
		prop_6 = (ImageView) findViewById(R.id.obj_6);
		prop_7 = (ImageView) findViewById(R.id.obj_7);
		prop_8 = (ImageView) findViewById(R.id.obj_8);
		prop_9 = (ImageView) findViewById(R.id.obj_9);
		prop_10 = (ImageView) findViewById(R.id.obj_10);
		prop_11 = (ImageView) findViewById(R.id.obj_11);
		prop_12 = (ImageView) findViewById(R.id.obj_12);

		conteneur_1_1 = (RelativeLayout) findViewById(R.id.conteneur_1_1);
		conteneur_1_2 = (RelativeLayout) findViewById(R.id.conteneur_1_2);
		conteneur_1_3 = (RelativeLayout) findViewById(R.id.conteneur_1_3);
		conteneur_1_4 = (RelativeLayout) findViewById(R.id.conteneur_1_4);
		conteneur_1_5 = (RelativeLayout) findViewById(R.id.conteneur_1_5);
		conteneur_1_6 = (RelativeLayout) findViewById(R.id.conteneur_1_6);

		conteneur_2_1 = (RelativeLayout) findViewById(R.id.conteneur_2_1);
		conteneur_2_2 = (RelativeLayout) findViewById(R.id.conteneur_2_2);
		conteneur_2_3 = (RelativeLayout) findViewById(R.id.conteneur_2_3);
		conteneur_2_4 = (RelativeLayout) findViewById(R.id.conteneur_2_4);
		conteneur_2_5 = (RelativeLayout) findViewById(R.id.conteneur_2_5);
		conteneur_2_6 = (RelativeLayout) findViewById(R.id.conteneur_2_6);
		
		conteneur_3_1 = (RelativeLayout) findViewById(R.id.conteneur_3_1);
		conteneur_3_2 = (RelativeLayout) findViewById(R.id.conteneur_3_2);
		conteneur_3_3 = (RelativeLayout) findViewById(R.id.conteneur_3_3);
		conteneur_3_4 = (RelativeLayout) findViewById(R.id.conteneur_3_4);
		conteneur_3_5 = (RelativeLayout) findViewById(R.id.conteneur_3_5);
		conteneur_3_6 = (RelativeLayout) findViewById(R.id.conteneur_3_6);

		zd_1 = (RelativeLayout) findViewById(R.id.zd_1);
		zd_2 = (RelativeLayout) findViewById(R.id.zd_2);
		zd_3 = (RelativeLayout) findViewById(R.id.zd_3);
		zd_4 = (RelativeLayout) findViewById(R.id.zd_4);
		zd_5 = (RelativeLayout) findViewById(R.id.zd_5);
		zd_6 = (RelativeLayout) findViewById(R.id.zd_6);
		zd_7 = (RelativeLayout) findViewById(R.id.zd_7);
		zd_8 = (RelativeLayout) findViewById(R.id.zd_8);
		zd_9 = (RelativeLayout) findViewById(R.id.zd_9);
		zd_10 = (RelativeLayout) findViewById(R.id.zd_10);
		zd_11 = (RelativeLayout) findViewById(R.id.zd_11);
		zd_12 = (RelativeLayout) findViewById(R.id.zd_12);

		Animation fadeIn = new AlphaAnimation(0, 1);
		fadeIn.setInterpolator(new DecelerateInterpolator()); // add this
		fadeIn.setDuration(1000);

		Animation fadeOut = new AlphaAnimation(1, 0);
		fadeOut.setInterpolator(new AccelerateInterpolator()); // and this
		fadeOut.setStartOffset(2500);// 3500
		fadeOut.setDuration(1000);

		animation = new AnimationSet(true); // change to false
		animation.addAnimation(fadeIn);
		animation.addAnimation(fadeOut);

		super.onCreate(savedInstanceState);

		prop_1.setOnTouchListener(new OnTouchListener() {

			public boolean onTouch(View view, MotionEvent motionEvent) {

				if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
					DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(
							view);

					view.startDrag(data, shadowBuilder, view, 0);
					view.setVisibility(View.INVISIBLE);

					return true;
				}

				else {
					prop_1.setVisibility(View.VISIBLE);
					return false;
				}
			}
		});
		prop_2.setOnTouchListener(new OnTouchListener() {

			public boolean onTouch(View view, MotionEvent motionEvent) {

				if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
					DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(
							view);

					view.startDrag(data, shadowBuilder, view, 0);
					view.setVisibility(View.INVISIBLE);

					return true;
				}

				else {
					prop_2.setVisibility(View.VISIBLE);
					return false;
				}
			}
		});
		prop_3.setOnTouchListener(new OnTouchListener() {

			public boolean onTouch(View view, MotionEvent motionEvent) {

				if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
					DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(
							view);

					view.startDrag(data, shadowBuilder, view, 0);
					view.setVisibility(View.INVISIBLE);

					return true;
				}

				else {
					prop_3.setVisibility(View.VISIBLE);
					return false;
				}
			}
		});
		prop_4.setOnTouchListener(new OnTouchListener() {

			public boolean onTouch(View view, MotionEvent motionEvent) {

				if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
					DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(
							view);

					view.startDrag(data, shadowBuilder, view, 0);
					view.setVisibility(View.INVISIBLE);

					return true;
				}

				else {
					prop_4.setVisibility(View.VISIBLE);
					return false;
				}
			}
		});
		prop_5.setOnTouchListener(new OnTouchListener() {

			public boolean onTouch(View view, MotionEvent motionEvent) {

				if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
					DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(
							view);

					view.startDrag(data, shadowBuilder, view, 0);
					view.setVisibility(View.INVISIBLE);

					return true;
				}

				else {
					prop_5.setVisibility(View.VISIBLE);
					return false;
				}
			}
		});
		prop_6.setOnTouchListener(new OnTouchListener() {

			public boolean onTouch(View view, MotionEvent motionEvent) {

				if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
					DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(
							view);

					view.startDrag(data, shadowBuilder, view, 0);
					view.setVisibility(View.INVISIBLE);

					return true;
				}

				else {
					prop_6.setVisibility(View.VISIBLE);
					return false;
				}
			}
		});
		prop_7.setOnTouchListener(new OnTouchListener() {

			public boolean onTouch(View view, MotionEvent motionEvent) {

				if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
					DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(
							view);

					view.startDrag(data, shadowBuilder, view, 0);
					view.setVisibility(View.INVISIBLE);

					return true;
				}

				else {
					prop_7.setVisibility(View.VISIBLE);
					return false;
				}
			}
		});
		prop_8.setOnTouchListener(new OnTouchListener() {

			public boolean onTouch(View view, MotionEvent motionEvent) {

				if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
					DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(
							view);

					view.startDrag(data, shadowBuilder, view, 0);
					view.setVisibility(View.INVISIBLE);

					return true;
				}

				else {
					prop_8.setVisibility(View.VISIBLE);
					return false;
				}
			}
		});
		prop_9.setOnTouchListener(new OnTouchListener() {

			public boolean onTouch(View view, MotionEvent motionEvent) {

				if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
					DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(
							view);

					view.startDrag(data, shadowBuilder, view, 0);
					view.setVisibility(View.INVISIBLE);

					return true;
				}

				else {
					prop_9.setVisibility(View.VISIBLE);
					return false;
				}
			}
		});
		
		prop_10.setOnTouchListener(new OnTouchListener() {

			public boolean onTouch(View view, MotionEvent motionEvent) {

				if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
					DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(
							view);

					view.startDrag(data, shadowBuilder, view, 0);
					view.setVisibility(View.INVISIBLE);

					return true;
				}

				else {
					prop_10.setVisibility(View.VISIBLE);
					return false;
				}
			}
		});
		prop_11.setOnTouchListener(new OnTouchListener() {

			public boolean onTouch(View view, MotionEvent motionEvent) {

				if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
					DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(
							view);

					view.startDrag(data, shadowBuilder, view, 0);
					view.setVisibility(View.INVISIBLE);

					return true;
				}

				else {
					prop_11.setVisibility(View.VISIBLE);
					return false;
				}
			}
		});
		
		prop_12.setOnTouchListener(new OnTouchListener() {

			public boolean onTouch(View view, MotionEvent motionEvent) {

				if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
					DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(
							view);

					view.startDrag(data, shadowBuilder, view, 0);
					view.setVisibility(View.INVISIBLE);

					return true;
				}

				else {
					prop_12.setVisibility(View.VISIBLE);
					return false;
				}
			}
		});

		conteneur_1_1.setOnDragListener(new OnDragListener() {

			public boolean onDrag(View v, DragEvent event) {
				switch (event.getAction()) {
				case DragEvent.ACTION_DRAG_STARTED:
					break;
				case DragEvent.ACTION_DRAG_ENTERED:
					break;
				case DragEvent.ACTION_DRAG_EXITED:
					break;
				case DragEvent.ACTION_DROP:
					View view4 = (View) event.getLocalState();
					ViewGroup owner = (ViewGroup) view4.getParent();
					owner.removeView(view4);
					RelativeLayout container = (RelativeLayout) v;
					container.addView(view4);
					view4.setVisibility(View.VISIBLE);
					conteneur_1_1.setEnabled(false);
					view4.setEnabled(false);
					if (view4 == prop_2 || view4 == prop_5) {
						_conteneur_1_1 = true;
						cont_1++;
					}
					test++;
					__conteneur_1_1 = true;
					break;
				case DragEvent.ACTION_DRAG_ENDED:
				default:
					break;
				}
				return true;
			}
		});

		conteneur_1_2.setOnDragListener(new OnDragListener() {

			public boolean onDrag(View v, DragEvent event) {
				switch (event.getAction()) {
				case DragEvent.ACTION_DRAG_STARTED:
					break;
				case DragEvent.ACTION_DRAG_ENTERED:
					break;
				case DragEvent.ACTION_DRAG_EXITED:
					break;
				case DragEvent.ACTION_DROP:
					View view4 = (View) event.getLocalState();
					ViewGroup owner = (ViewGroup) view4.getParent();
					owner.removeView(view4);
					RelativeLayout container = (RelativeLayout) v;
					container.addView(view4);
					conteneur_1_2.setEnabled(false);
					view4.setVisibility(View.VISIBLE);
					view4.setEnabled(false);
					if (view4 == prop_2 || view4 == prop_5) {
						_conteneur_1_2 = true;
						cont_1++;
					}
					test++;
					__conteneur_1_2 = true;
					break;
				case DragEvent.ACTION_DRAG_ENDED:
				default:
					break;
				}
				return true;
			}
		});
		conteneur_1_3.setOnDragListener(new OnDragListener() {

			public boolean onDrag(View v, DragEvent event) {
				switch (event.getAction()) {
				case DragEvent.ACTION_DRAG_STARTED:
					break;
				case DragEvent.ACTION_DRAG_ENTERED:
					break;
				case DragEvent.ACTION_DRAG_EXITED:
					break;
				case DragEvent.ACTION_DROP:
					View view4 = (View) event.getLocalState();
					ViewGroup owner = (ViewGroup) view4.getParent();
					owner.removeView(view4);
					RelativeLayout container = (RelativeLayout) v;
					container.addView(view4);
					view4.setVisibility(View.VISIBLE);
					view4.setEnabled(false);
					conteneur_1_3.setEnabled(false);
					if (view4 == prop_2 || view4 == prop_5) {
						_conteneur_1_3 = true;
						cont_1++;
					}
					__conteneur_1_3 = true;
					test++;

					break;
				case DragEvent.ACTION_DRAG_ENDED:
				default:
					break;
				}
				return true;
			}
		});
		conteneur_1_4.setOnDragListener(new OnDragListener() {

			public boolean onDrag(View v, DragEvent event) {
				switch (event.getAction()) {
				case DragEvent.ACTION_DRAG_STARTED:
					break;
				case DragEvent.ACTION_DRAG_ENTERED:
					break;
				case DragEvent.ACTION_DRAG_EXITED:
					break;
				case DragEvent.ACTION_DROP:
					View view4 = (View) event.getLocalState();
					ViewGroup owner = (ViewGroup) view4.getParent();
					owner.removeView(view4);
					RelativeLayout container = (RelativeLayout) v;
					container.addView(view4);
					view4.setVisibility(View.VISIBLE);
					view4.setEnabled(false);
					conteneur_1_4.setEnabled(false);
					if (view4 == prop_2 || view4 == prop_5) {
						_conteneur_1_4 = true;
						cont_1++;
					}
					__conteneur_1_4 = true;
					test++;

					break;
				case DragEvent.ACTION_DRAG_ENDED:
				default:
					break;
				}
				return true;
			}
		});
		conteneur_1_5.setOnDragListener(new OnDragListener() {

			public boolean onDrag(View v, DragEvent event) {
				switch (event.getAction()) {
				case DragEvent.ACTION_DRAG_STARTED:
					break;
				case DragEvent.ACTION_DRAG_ENTERED:
					break;
				case DragEvent.ACTION_DRAG_EXITED:
					break;
				case DragEvent.ACTION_DROP:
					View view4 = (View) event.getLocalState();
					ViewGroup owner = (ViewGroup) view4.getParent();
					owner.removeView(view4);
					RelativeLayout container = (RelativeLayout) v;
					container.addView(view4);
					view4.setVisibility(View.VISIBLE);
					view4.setEnabled(false);
					conteneur_1_5.setEnabled(false);
					if (view4 == prop_2 || view4 == prop_5) {
						_conteneur_1_5 = true;
						cont_1++;
					}
					__conteneur_1_5 = true;
					test++;

					break;
				case DragEvent.ACTION_DRAG_ENDED:
				default:
					break;
				}
				return true;
			}
		});
		conteneur_1_6.setOnDragListener(new OnDragListener() {

			public boolean onDrag(View v, DragEvent event) {
				switch (event.getAction()) {
				case DragEvent.ACTION_DRAG_STARTED:
					break;
				case DragEvent.ACTION_DRAG_ENTERED:
					break;
				case DragEvent.ACTION_DRAG_EXITED:
					break;
				case DragEvent.ACTION_DROP:
					View view4 = (View) event.getLocalState();
					ViewGroup owner = (ViewGroup) view4.getParent();
					owner.removeView(view4);
					RelativeLayout container = (RelativeLayout) v;
					container.addView(view4);
					view4.setVisibility(View.VISIBLE);
					view4.setEnabled(false);
					conteneur_1_6.setEnabled(false);
					if (view4 == prop_2 || view4 == prop_5) {
						_conteneur_1_6 = true;
						cont_1++;
					}
					__conteneur_1_6 = true;
					test++;

					break;
				case DragEvent.ACTION_DRAG_ENDED:
				default:
					break;
				}
				return true;
			}
		});

		conteneur_2_1.setOnDragListener(new OnDragListener() {

			public boolean onDrag(View v, DragEvent event) {
				switch (event.getAction()) {
				case DragEvent.ACTION_DRAG_STARTED:
					break;
				case DragEvent.ACTION_DRAG_ENTERED:
					break;
				case DragEvent.ACTION_DRAG_EXITED:
					break;
				case DragEvent.ACTION_DROP:
					View view4 = (View) event.getLocalState();
					ViewGroup owner = (ViewGroup) view4.getParent();
					owner.removeView(view4);
					RelativeLayout container = (RelativeLayout) v;
					container.addView(view4);
					view4.setVisibility(View.VISIBLE);
					view4.setEnabled(false);
					conteneur_2_1.setEnabled(false);
					if (view4 == prop_9 || view4 == prop_6 || view4 == prop_10
							|| view4 == prop_12) {
						_conteneur_2_1 = true;
						cont_2++;
					}
					__conteneur_2_1 = true;
					test++;

					break;
				case DragEvent.ACTION_DRAG_ENDED:
				default:
					break;
				}
				return true;
			}
		});
		conteneur_2_2.setOnDragListener(new OnDragListener() {

			public boolean onDrag(View v, DragEvent event) {
				switch (event.getAction()) {
				case DragEvent.ACTION_DRAG_STARTED:
					break;
				case DragEvent.ACTION_DRAG_ENTERED:
					break;
				case DragEvent.ACTION_DRAG_EXITED:
					break;
				case DragEvent.ACTION_DROP:
					View view4 = (View) event.getLocalState();
					ViewGroup owner = (ViewGroup) view4.getParent();
					owner.removeView(view4);
					RelativeLayout container = (RelativeLayout) v;
					container.addView(view4);
					view4.setVisibility(View.VISIBLE);
					view4.setEnabled(false);
					conteneur_2_2.setEnabled(false);
					if (view4 == prop_9 || view4 == prop_6 || view4 == prop_10
							|| view4 == prop_12) {
						_conteneur_2_2 = true;
						cont_2++;
					}
					test++;
					__conteneur_2_2 = true;

					break;
				case DragEvent.ACTION_DRAG_ENDED:
				default:
					break;
				}
				return true;
			}
		});
		conteneur_2_3.setOnDragListener(new OnDragListener() {

			public boolean onDrag(View v, DragEvent event) {
				switch (event.getAction()) {
				case DragEvent.ACTION_DRAG_STARTED:
					break;
				case DragEvent.ACTION_DRAG_ENTERED:
					break;
				case DragEvent.ACTION_DRAG_EXITED:
					break;
				case DragEvent.ACTION_DROP:
					View view4 = (View) event.getLocalState();
					ViewGroup owner = (ViewGroup) view4.getParent();
					owner.removeView(view4);
					RelativeLayout container = (RelativeLayout) v;
					container.addView(view4);
					view4.setVisibility(View.VISIBLE);
					view4.setEnabled(false);
					conteneur_2_3.setEnabled(false);
					if (view4 == prop_9 || view4 == prop_6 || view4 == prop_10
							|| view4 == prop_12) {
						_conteneur_2_3 = true;
						cont_2++;
					}
					test++;
					__conteneur_2_3 = true;

					break;
				case DragEvent.ACTION_DRAG_ENDED:
				default:
					break;
				}
				return true;
			}
		});
		conteneur_2_4.setOnDragListener(new OnDragListener() {

			public boolean onDrag(View v, DragEvent event) {
				switch (event.getAction()) {
				case DragEvent.ACTION_DRAG_STARTED:
					break;
				case DragEvent.ACTION_DRAG_ENTERED:
					break;
				case DragEvent.ACTION_DRAG_EXITED:
					break;
				case DragEvent.ACTION_DROP:
					View view4 = (View) event.getLocalState();
					ViewGroup owner = (ViewGroup) view4.getParent();
					owner.removeView(view4);
					RelativeLayout container = (RelativeLayout) v;
					container.addView(view4);
					view4.setVisibility(View.VISIBLE);
					view4.setEnabled(false);
					conteneur_2_4.setEnabled(false);
					if (view4 == prop_9 || view4 == prop_6 || view4 == prop_10
							|| view4 == prop_12) {
						_conteneur_2_4 = true;
						cont_2++;
					}
					test++;
					__conteneur_2_4 = true;

					break;
				case DragEvent.ACTION_DRAG_ENDED:
				default:
					break;
				}
				return true;
			}
		});
		conteneur_2_5.setOnDragListener(new OnDragListener() {

			public boolean onDrag(View v, DragEvent event) {
				switch (event.getAction()) {
				case DragEvent.ACTION_DRAG_STARTED:
					break;
				case DragEvent.ACTION_DRAG_ENTERED:
					break;
				case DragEvent.ACTION_DRAG_EXITED:
					break;
				case DragEvent.ACTION_DROP:
					View view4 = (View) event.getLocalState();
					ViewGroup owner = (ViewGroup) view4.getParent();
					owner.removeView(view4);
					RelativeLayout container = (RelativeLayout) v;
					container.addView(view4);
					view4.setVisibility(View.VISIBLE);
					view4.setEnabled(false);
					conteneur_2_5.setEnabled(false);
					if (view4 == prop_9 || view4 == prop_6 || view4 == prop_10
							|| view4 == prop_12) {
						_conteneur_2_5 = true;
						cont_2++;
					}
					test++;
					__conteneur_2_5 = true;

					break;
				case DragEvent.ACTION_DRAG_ENDED:
				default:
					break;
				}
				return true;
			}
		});
		conteneur_2_6.setOnDragListener(new OnDragListener() {

			public boolean onDrag(View v, DragEvent event) {
				switch (event.getAction()) {
				case DragEvent.ACTION_DRAG_STARTED:
					break;
				case DragEvent.ACTION_DRAG_ENTERED:
					break;
				case DragEvent.ACTION_DRAG_EXITED:
					break;
				case DragEvent.ACTION_DROP:
					View view4 = (View) event.getLocalState();
					ViewGroup owner = (ViewGroup) view4.getParent();
					owner.removeView(view4);
					RelativeLayout container = (RelativeLayout) v;
					container.addView(view4);
					view4.setVisibility(View.VISIBLE);
					view4.setEnabled(false);
					conteneur_2_6.setEnabled(false);
					if (view4 == prop_9 || view4 == prop_6 || view4 == prop_10
							|| view4 == prop_12) {
						_conteneur_2_6 = true;
						cont_2++;
					}
					test++;
					__conteneur_2_6 = true;

					break;
				case DragEvent.ACTION_DRAG_ENDED:
				default:
					break;
				}
				return true;
			}
		});
		
		conteneur_3_1.setOnDragListener(new OnDragListener() {

			public boolean onDrag(View v, DragEvent event) {
				switch (event.getAction()) {
				case DragEvent.ACTION_DRAG_STARTED:
					break;
				case DragEvent.ACTION_DRAG_ENTERED:
					break;
				case DragEvent.ACTION_DRAG_EXITED:
					break;
				case DragEvent.ACTION_DROP:
					View view4 = (View) event.getLocalState();
					ViewGroup owner = (ViewGroup) view4.getParent();
					owner.removeView(view4);
					RelativeLayout container = (RelativeLayout) v;
					container.addView(view4);
					view4.setVisibility(View.VISIBLE);
					view4.setEnabled(false);
					conteneur_3_1.setEnabled(false);
					if (view4 == prop_1 || view4 == prop_4 || view4 == prop_3
							|| view4 == prop_7|| view4 == prop_8|| view4 == prop_11) {
						_conteneur_3_1 = true;
						cont_3++;
					}
					__conteneur_3_1 = true;
					test++;

					break;
				case DragEvent.ACTION_DRAG_ENDED:
				default:
					break;
				}
				return true;
			}
		});
		conteneur_3_2.setOnDragListener(new OnDragListener() {

			public boolean onDrag(View v, DragEvent event) {
				switch (event.getAction()) {
				case DragEvent.ACTION_DRAG_STARTED:
					break;
				case DragEvent.ACTION_DRAG_ENTERED:
					break;
				case DragEvent.ACTION_DRAG_EXITED:
					break;
				case DragEvent.ACTION_DROP:
					View view4 = (View) event.getLocalState();
					ViewGroup owner = (ViewGroup) view4.getParent();
					owner.removeView(view4);
					RelativeLayout container = (RelativeLayout) v;
					container.addView(view4);
					view4.setVisibility(View.VISIBLE);
					view4.setEnabled(false);
					conteneur_3_2.setEnabled(false);
					if (view4 == prop_1 || view4 == prop_4 || view4 == prop_3
							|| view4 == prop_7|| view4 == prop_8|| view4 == prop_11) {
						_conteneur_3_2 = true;
						cont_3++;
					}
					test++;
					__conteneur_3_2 = true;

					break;
				case DragEvent.ACTION_DRAG_ENDED:
				default:
					break;
				}
				return true;
			}
		});
		conteneur_3_3.setOnDragListener(new OnDragListener() {

			public boolean onDrag(View v, DragEvent event) {
				switch (event.getAction()) {
				case DragEvent.ACTION_DRAG_STARTED:
					break;
				case DragEvent.ACTION_DRAG_ENTERED:
					break;
				case DragEvent.ACTION_DRAG_EXITED:
					break;
				case DragEvent.ACTION_DROP:
					View view4 = (View) event.getLocalState();
					ViewGroup owner = (ViewGroup) view4.getParent();
					owner.removeView(view4);
					RelativeLayout container = (RelativeLayout) v;
					container.addView(view4);
					view4.setVisibility(View.VISIBLE);
					view4.setEnabled(false);
					conteneur_3_3.setEnabled(false);
					if (view4 == prop_1 || view4 == prop_4 || view4 == prop_3
							|| view4 == prop_7|| view4 == prop_8|| view4 == prop_11) {
						_conteneur_3_3 = true;
						cont_3++;
					}
					test++;
					__conteneur_3_3 = true;

					break;
				case DragEvent.ACTION_DRAG_ENDED:
				default:
					break;
				}
				return true;
			}
		});
		conteneur_3_4.setOnDragListener(new OnDragListener() {

			public boolean onDrag(View v, DragEvent event) {
				switch (event.getAction()) {
				case DragEvent.ACTION_DRAG_STARTED:
					break;
				case DragEvent.ACTION_DRAG_ENTERED:
					break;
				case DragEvent.ACTION_DRAG_EXITED:
					break;
				case DragEvent.ACTION_DROP:
					View view4 = (View) event.getLocalState();
					ViewGroup owner = (ViewGroup) view4.getParent();
					owner.removeView(view4);
					RelativeLayout container = (RelativeLayout) v;
					container.addView(view4);
					view4.setVisibility(View.VISIBLE);
					view4.setEnabled(false);
					conteneur_3_4.setEnabled(false);
					if (view4 == prop_1 || view4 == prop_4 || view4 == prop_3
							|| view4 == prop_7|| view4 == prop_8|| view4 == prop_11) {
						_conteneur_3_4 = true;
						cont_3++;
					}
					test++;
					__conteneur_3_4 = true;

					break;
				case DragEvent.ACTION_DRAG_ENDED:
				default:
					break;
				}
				return true;
			}
		});
		conteneur_3_5.setOnDragListener(new OnDragListener() {

			public boolean onDrag(View v, DragEvent event) {
				switch (event.getAction()) {
				case DragEvent.ACTION_DRAG_STARTED:
					break;
				case DragEvent.ACTION_DRAG_ENTERED:
					break;
				case DragEvent.ACTION_DRAG_EXITED:
					break;
				case DragEvent.ACTION_DROP:
					View view4 = (View) event.getLocalState();
					ViewGroup owner = (ViewGroup) view4.getParent();
					owner.removeView(view4);
					RelativeLayout container = (RelativeLayout) v;
					container.addView(view4);
					view4.setVisibility(View.VISIBLE);
					view4.setEnabled(false);
					conteneur_3_5.setEnabled(false);
					if (view4 == prop_1 || view4 == prop_4 || view4 == prop_3
							|| view4 == prop_7|| view4 == prop_8|| view4 == prop_11) {
						_conteneur_3_5 = true;
						cont_3++;
					}
					test++;
					__conteneur_3_5 = true;

					break;
				case DragEvent.ACTION_DRAG_ENDED:
				default:
					break;
				}
				return true;
			}
		});
		conteneur_3_6.setOnDragListener(new OnDragListener() {

			public boolean onDrag(View v, DragEvent event) {
				switch (event.getAction()) {
				case DragEvent.ACTION_DRAG_STARTED:
					break;
				case DragEvent.ACTION_DRAG_ENTERED:
					break;
				case DragEvent.ACTION_DRAG_EXITED:
					break;
				case DragEvent.ACTION_DROP:
					View view4 = (View) event.getLocalState();
					ViewGroup owner = (ViewGroup) view4.getParent();
					owner.removeView(view4);
					RelativeLayout container = (RelativeLayout) v;
					container.addView(view4);
					view4.setVisibility(View.VISIBLE);
					view4.setEnabled(false);
					conteneur_3_6.setEnabled(false);
					if (view4 == prop_1 || view4 == prop_4 || view4 == prop_3
							|| view4 == prop_7|| view4 == prop_8|| view4 == prop_11) {
						_conteneur_3_6 = true;
						cont_3++;
					}
					test++;
					__conteneur_3_6 = true;

					break;
				case DragEvent.ACTION_DRAG_ENDED:
				default:
					break;
				}
				return true;
			}
		});
		imgVer.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Random rand = new Random();
				randomInt = rand.nextInt();
				imgCorr.setBackgroundDrawable(null);
				imgCorr.setBackgroundDrawable(null);
				imgCorr.setBackgroundDrawable(null);
				try_stop_mp(mp);

				if (test < 11) {

					imgCorr.setVisibility(1);
					imgCorr.setBackgroundResource(R.drawable.mascotte_incomplet);
					imgCorr.startAnimation(animation);
					imgCorr.setVisibility(View.INVISIBLE);
					mp = MediaPlayer.create(getApplicationContext(),
							R.raw.mascotte_incomplet);
					mp.start();
				} else if (cont_1 == 2 && cont_2 == 4 && cont_3 == 6) {

					imgCorr.setVisibility(1);
					imgCorr.setBackgroundResource(R.drawable.mascotte_vrai_1);
					imgCorr.startAnimation(animation);
					imgCorr.setVisibility(View.INVISIBLE);
					mp = MediaPlayer.create(getApplicationContext(),
							R.raw.mascotte_vrai_1);
					mp.start();

				} else {
					imgCorr.setVisibility(1);
					imgCorr.setBackgroundResource(R.drawable.mascotte_faux_2);
					imgCorr.startAnimation(animation);
					imgCorr.setVisibility(View.INVISIBLE);

					mp = MediaPlayer.create(getApplicationContext(),
							R.raw.mascotte_faux_2);
					mp.start();

					Message msg = new Message();
					msg.what = STOP;
					Handler.sendMessageDelayed(msg, active);
				}

				/*
				 * if (_rep1 == true) {
				 * rep1.setBackgroundResource(R.drawable.img1_f_m2_ex1); }
				 * 
				 * if (_rep2 == true) {
				 * rep2.setBackgroundResource(R.drawable.img2_v_m2_ex1); }
				 * 
				 * if (_rep3 == true) {
				 * rep3.setBackgroundResource(R.drawable.img3_f_m2_ex1); }
				 * 
				 * if (_rep4 == true) {
				 * rep4.setBackgroundResource(R.drawable.img4_v_m2_ex1); } if
				 * (_rep5 == true) {
				 * rep5.setBackgroundResource(R.drawable.img5_v_m2_ex1); } if
				 * (_rep6 == true) {
				 * rep6.setBackgroundResource(R.drawable.img6_f_m2_ex1); } if
				 * (_rep7 == true) {
				 * rep7.setBackgroundResource(R.drawable.img7_v_m2_ex1); } if
				 * (_rep8 == true) {
				 * rep8.setBackgroundResource(R.drawable.img8_v_m2_ex1); }
				 * 
				 * if ((_rep1 == false && _rep3 == false && _rep6 == false) &&
				 * (_rep2 == false || _rep4 == false || _rep5 == false || _rep7
				 * == false || _rep8 == false)) { imgCorr.setVisibility(1);
				 * imgCorr.setBackgroundResource(R.drawable.mascotte_incomplet);
				 * imgCorr.startAnimation(animation);
				 * imgCorr.setVisibility(View.INVISIBLE); mp =
				 * MediaPlayer.create(getApplicationContext(),
				 * R.raw.mascotte_incomplet); mp.start();
				 * 
				 * }
				 * 
				 * else if (_rep2 == true && _rep4 == true && _rep5 == true &&
				 * _rep7 == false && _rep8 == false) { imgCorr.setVisibility(1);
				 * imgCorr.setBackgroundResource(R.drawable.mascotte_vrai_1);
				 * imgCorr.startAnimation(animation);
				 * imgCorr.setVisibility(View.INVISIBLE); mp =
				 * MediaPlayer.create(getApplicationContext(),
				 * R.raw.mascotte_vrai_1); mp.start();
				 * 
				 * rep1.setEnabled(false); rep2.setEnabled(false);
				 * rep3.setEnabled(false); rep4.setEnabled(false);
				 * 
				 * } else if (_rep1 == true && _rep3 == true && _rep4 == true &&
				 * _rep2 == true && _rep5 == true && _rep6 == true && _rep7 ==
				 * true && _rep8 == true) { imgCorr.setVisibility(1);
				 * imgCorr.setBackgroundResource(R.drawable.mascotte_faux_2);
				 * imgCorr.startAnimation(animation);
				 * imgCorr.setVisibility(View.INVISIBLE);
				 * 
				 * mp = MediaPlayer.create(getApplicationContext(),
				 * R.raw.mascotte_faux_2); mp.start();
				 * mp.setOnCompletionListener(new OnCompletionListener() {
				 * 
				 * @Override public void onCompletion(MediaPlayer mp) { _rep1 =
				 * false; _rep2 = false; _rep3 = false; _rep4 = false; _rep5 =
				 * false; _rep6 = false; _rep7 = false; _rep8 = false;
				 * rep1.setBackgroundResource(R.drawable.img1_m2_ex1);
				 * rep2.setBackgroundResource(R.drawable.img2_m2_ex1);
				 * rep3.setBackgroundResource(R.drawable.img3_m2_ex1);
				 * rep4.setBackgroundResource(R.drawable.img4_m2_ex1);
				 * rep5.setBackgroundResource(R.drawable.img5_m2_ex1);
				 * rep6.setBackgroundResource(R.drawable.img6_m2_ex1);
				 * rep7.setBackgroundResource(R.drawable.img7_m2_ex1);
				 * rep8.setBackgroundResource(R.drawable.img8_m2_ex1); } }); }
				 * else { imgCorr.setVisibility(1);
				 * imgCorr.setBackgroundResource(R.drawable.mascotte_faux_2);
				 * imgCorr.startAnimation(animation);
				 * imgCorr.setVisibility(View.INVISIBLE);
				 * 
				 * mp = MediaPlayer.create(getApplicationContext(),
				 * R.raw.mascotte_faux_2); mp.start();
				 * mp.setOnCompletionListener(new OnCompletionListener() {
				 * 
				 * @Override public void onCompletion(MediaPlayer mp) { _rep1 =
				 * false; _rep3 = false; _rep6 = false;
				 * rep1.setBackgroundResource(R.drawable.img1_m2_ex1);
				 * rep3.setBackgroundResource(R.drawable.img3_m2_ex1);
				 * rep6.setBackgroundResource(R.drawable.img6_m2_ex1); } }); }
				 * 
				 * /* switch (verifierReponse()) { case 0:
				 * imgCorr.setVisibility(1);
				 * imgCorr.setBackgroundResource(R.drawable.mascotte_incomplet);
				 * imgCorr.startAnimation(animation);
				 * imgCorr.setVisibility(View.INVISIBLE); mp =
				 * MediaPlayer.create(getApplicationContext(),
				 * R.raw.mascotte_incomplet); mp.start(); break; case 1:
				 * imgCorr.setVisibility(1);
				 * imgCorr.setBackgroundResource(R.drawable.mascotte_faux_2);
				 * imgCorr.startAnimation(animation);
				 * imgCorr.setVisibility(View.INVISIBLE);
				 * 
				 * mp = MediaPlayer.create(getApplicationContext(),
				 * R.raw.mascotte_faux_2); mp.start(); break; case 2:
				 * imgCorr.setVisibility(1);
				 * imgCorr.setBackgroundResource(R.drawable.mascotte_vrai_1);
				 * imgCorr.startAnimation(animation);
				 * imgCorr.setVisibility(View.INVISIBLE);
				 * 
				 * mp = MediaPlayer.create(getApplicationContext(),
				 * R.raw.mascotte_vrai_1); mp.start(); break; }
				 */
			}
		});

		img2.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				try_stop_mp(mp);
				Intent i2 = new Intent(getApplicationContext(), Page1_m3.class);
				startActivity(i2);
				act.finish();
			}
		});

		home.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				try_stop_mp(mp);
				startActivity(new Intent(getApplicationContext(), Act_Modules.class));

				act.finish();

			}
		});
		exit.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				try_stop_mp(mp);
				finish();

			}
		});
		ex3.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				try_stop_mp(mp);
				startActivity(new Intent(getApplicationContext(), Ex3_m3.class));

				act.finish();

			}
		});
		ex1.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				try_stop_mp(mp);
				startActivity(new Intent(getApplicationContext(), Ex1_m3.class));

				act.finish();

			}
		});

		ex4.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				try_stop_mp(mp);
				startActivity(new Intent(getApplicationContext(), Ex4_m3.class));

				act.finish();
			}
		});

		ex5.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				try_stop_mp(mp);
				startActivity(new Intent(getApplicationContext(), Ex5_m3.class));

				act.finish();
			}
		});

		ex6.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				try_stop_mp(mp);
				if (isOnline()) {
					Intent intUrl = new Intent(Intent.ACTION_VIEW);
					intUrl.setData(Uri.parse("http://google.com"));
					startActivity(intUrl);
				} else {
					Toast.makeText(getApplicationContext(),
							"V�rifier Votre Connexion ", Toast.LENGTH_SHORT)
							.show();
				}
				// Intent i2 = new Intent(getApplicationContext(),
				// Tri_1_Mod_1_Ex_6.class);
				// startActivity(i2);
				// act.finish();
			}
		});

		ex7.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				try_stop_mp(mp);
				if (isOnline()) {
					Intent intUrl = new Intent(Intent.ACTION_VIEW);
					intUrl.setData(Uri.parse("http://google.com"));
					startActivity(intUrl);
				} else {
					Toast.makeText(getApplicationContext(),
							"V�rifier Votre Connexion ", Toast.LENGTH_SHORT)
							.show();
				}
				// Intent i2 = new Intent(getApplicationContext(),
				// Tri_1_Mod_1_Ex_7.class);
				// startActivity(i2);
				// act.finish();
			}
		});

		ex8.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				try_stop_mp(mp);
				if (isOnline()) {
					Intent intUrl = new Intent(Intent.ACTION_VIEW);
					intUrl.setData(Uri.parse("http://google.com"));
					startActivity(intUrl);
				} else {
					Toast.makeText(getApplicationContext(),
							"V�rifier Votre Connexion ", Toast.LENGTH_SHORT)
							.show();
				}
				// Intent i2 = new Intent(getApplicationContext(),
				// Tri_1_Mod_1_Ex_8.class);
				// startActivity(i2);
				// act.finish();
			}
		});

		ex9.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				try_stop_mp(mp);
				if (isOnline()) {
					Intent intUrl = new Intent(Intent.ACTION_VIEW);
					intUrl.setData(Uri.parse("http://google.com"));
					startActivity(intUrl);
				} else {
					Toast.makeText(getApplicationContext(),
							"V�rifier Votre Connexion ", Toast.LENGTH_SHORT)
							.show();
				}
				// Intent i2 = new Intent(getApplicationContext(),
				// Tri_1_Mod_1_Ex_9.class);
				// startActivity(i2);
				// act.finish();
			}
		});
		imgCorr.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				imgCorr.setVisibility(View.INVISIBLE);

			}
		});

		rep.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				try_stop_mp(mp);
				finish();
				startActivity(getIntent());
			}
		});

		audio.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				try_stop_mp(mp);
				mp = new MediaPlayer();
				mp = MediaPlayer.create(Ex2_m3.this, R.raw.ex2_m3);
				mp.start();
			}
		});

	}

	public void try_stop_mp(MediaPlayer mp) {
		try {

			if (mp != null) {
				mp.stop();
				mp.release();
			}

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	// protected void onDestroy() {
	// mp.release();
	// mWakeLock.release();
	// super.onDestroy();
	// }

	protected void onRestart() {
		super.onRestart();
	}

	protected void onPause() {
		act.finish();
		super.onPause();
	}

	// ///////////////////////////////////////////////////////////////////////////////////////////
	public void onWindowFocusChanged(boolean hasFocus) {
		if (!scalingComplete) // only do this once
		{
			scaleContents(findViewById(R.id.contents),
					findViewById(R.id.container));
			scalingComplete = true;
		}
		super.onWindowFocusChanged(hasFocus);
	}

	/**
	 * Called when the views have been created. We override this in order to
	 * scale the UI, which we can't do before this.
	 */
	@Override
	public View onCreateView(String name, Context context, AttributeSet attrs) {
		View view = super.onCreateView(name, context, attrs);
		return view;
	}

	private void scaleContents(View rootView, View container) {
		float xScale = (float) container.getWidth() / rootView.getWidth();
		float yScale = (float) container.getHeight() / rootView.getHeight();
		float scale = Math.min(xScale, yScale);
		scaleViewAndChildren(rootView, scale);
	}

	public static void scaleViewAndChildren(View root, float scale) {
		ViewGroup.LayoutParams layoutParams = root.getLayoutParams();

		// Scale the view itself
		if (layoutParams.width != ViewGroup.LayoutParams.FILL_PARENT
				&& layoutParams.width != ViewGroup.LayoutParams.WRAP_CONTENT) {
			layoutParams.width *= scale;
		}
		if (layoutParams.height != ViewGroup.LayoutParams.FILL_PARENT
				&& layoutParams.height != ViewGroup.LayoutParams.WRAP_CONTENT) {
			layoutParams.height *= scale;
		}

		if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
			ViewGroup.MarginLayoutParams marginParams = (ViewGroup.MarginLayoutParams) layoutParams;
			marginParams.leftMargin *= scale;
			marginParams.rightMargin *= scale;
			marginParams.topMargin *= scale;
			marginParams.bottomMargin *= scale;
		}

		root.setLayoutParams(layoutParams);

		root.setPadding((int) (root.getPaddingLeft() * scale),
				(int) (root.getPaddingTop() * scale),
				(int) (root.getPaddingRight() * scale),
				(int) (root.getPaddingBottom() * scale));

		if (root instanceof TextView) {
			TextView textView = (TextView) root;
			textView.setTextSize(textView.getTextSize() * scale);
		}

		if (root instanceof ViewGroup) {
			ViewGroup groupView = (ViewGroup) root;
			for (int cnt = 0; cnt < groupView.getChildCount(); ++cnt)
				scaleViewAndChildren(groupView.getChildAt(cnt), scale);
		}
	}

	public boolean isOnline() {

		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}

	private Handler Handler = new Handler() {
		public void handleMessage(Message msg) {

			if (!_conteneur_1_1 && __conteneur_1_1 == true) {

				View v1 = conteneur_1_1.getChildAt(0);
				conteneur_1_1.removeView(v1);

				resetImage(v1);
				v1.setVisibility(View.VISIBLE);
				test--;

				conteneur_1_1.setEnabled(true);
				v1.setEnabled(true);
				__conteneur_1_1 = false;
			}
			if (!_conteneur_1_2 && __conteneur_1_2 == true) {

				View v1 = conteneur_1_2.getChildAt(0);
				conteneur_1_2.removeView(v1);

				resetImage(v1);
				v1.setVisibility(View.VISIBLE);
				test--;

				conteneur_1_2.setEnabled(true);
				v1.setEnabled(true);
				__conteneur_1_2 = false;
			}
			if (!_conteneur_1_3 && __conteneur_1_3 == true) {

				View v1 = conteneur_1_3.getChildAt(0);
				conteneur_1_3.removeView(v1);

				resetImage(v1);
				v1.setVisibility(View.VISIBLE);
				test--;

				conteneur_1_3.setEnabled(true);
				v1.setEnabled(true);
				__conteneur_1_3 = false;
			}
			if (!_conteneur_1_4 && __conteneur_1_4 == true) {

				View v1 = conteneur_1_4.getChildAt(0);
				conteneur_1_4.removeView(v1);

				resetImage(v1);
				v1.setVisibility(View.VISIBLE);
				test--;

				conteneur_1_4.setEnabled(true);
				v1.setEnabled(true);
				__conteneur_1_4 = false;
			}
			if (!_conteneur_1_5 && __conteneur_1_5 == true) {

				View v1 = conteneur_1_5.getChildAt(0);
				conteneur_1_5.removeView(v1);

				resetImage(v1);
				v1.setVisibility(View.VISIBLE);
				test--;

				conteneur_1_5.setEnabled(true);
				v1.setEnabled(true);
				__conteneur_1_5 = false;
			}
			if (!_conteneur_1_6 && __conteneur_1_6 == true) {

				View v1 = conteneur_1_6.getChildAt(0);
				conteneur_1_6.removeView(v1);

				resetImage(v1);
				v1.setVisibility(View.VISIBLE);
				test--;

				conteneur_1_6.setEnabled(true);
				v1.setEnabled(true);
				__conteneur_1_6 = false;
			}

			// 2

			if (!_conteneur_2_1 && __conteneur_2_1 == true) {

				View v1 = conteneur_2_1.getChildAt(0);
				conteneur_2_1.removeView(v1);

				resetImage(v1);
				v1.setVisibility(View.VISIBLE);
				test--;

				conteneur_2_1.setEnabled(true);
				v1.setEnabled(true);
				__conteneur_2_1 = false;
			}
			if (!_conteneur_2_2 && __conteneur_2_2 == true) {

				View v1 = conteneur_2_2.getChildAt(0);
				conteneur_2_2.removeView(v1);

				resetImage(v1);
				v1.setVisibility(View.VISIBLE);
				test--;

				conteneur_2_2.setEnabled(true);
				v1.setEnabled(true);
				__conteneur_2_2 = false;
			}
			if (!_conteneur_2_3 && __conteneur_2_3 == true) {

				View v1 = conteneur_2_3.getChildAt(0);
				conteneur_2_3.removeView(v1);

				resetImage(v1);
				v1.setVisibility(View.VISIBLE);
				test--;

				conteneur_2_3.setEnabled(true);
				v1.setEnabled(true);
				__conteneur_2_3 = false;
			}
			if (!_conteneur_2_4 && __conteneur_2_4 == true) {

				View v1 = conteneur_2_4.getChildAt(0);
				conteneur_2_4.removeView(v1);

				resetImage(v1);
				v1.setVisibility(View.VISIBLE);
				test--;

				conteneur_2_4.setEnabled(true);
				v1.setEnabled(true);
				__conteneur_2_4 = false;
			}
			if (!_conteneur_2_5 && __conteneur_2_5 == true) {

				View v1 = conteneur_2_5.getChildAt(0);
				conteneur_2_5.removeView(v1);

				resetImage(v1);
				v1.setVisibility(View.VISIBLE);
				test--;

				conteneur_2_5.setEnabled(true);
				v1.setEnabled(true);
				__conteneur_2_5 = false;
			}
			if (!_conteneur_2_6 && __conteneur_2_6 == true) {

				View v1 = conteneur_2_6.getChildAt(0);
				conteneur_2_6.removeView(v1);

				resetImage(v1);
				v1.setVisibility(View.VISIBLE);
				test--;

				conteneur_2_6.setEnabled(true);
				v1.setEnabled(true);
				__conteneur_2_6 = false;
			}

			
			if (!_conteneur_3_1 && __conteneur_3_1 == true) {

				View v1 = conteneur_3_1.getChildAt(0);
				conteneur_3_1.removeView(v1);

				resetImage(v1);
				v1.setVisibility(View.VISIBLE);
				test--;

				conteneur_3_1.setEnabled(true);
				v1.setEnabled(true);
				__conteneur_3_1 = false;
			}
			if (!_conteneur_3_2 && __conteneur_3_2 == true) {

				View v1 = conteneur_3_2.getChildAt(0);
				conteneur_3_2.removeView(v1);

				resetImage(v1);
				v1.setVisibility(View.VISIBLE);
				test--;

				conteneur_3_2.setEnabled(true);
				v1.setEnabled(true);
				__conteneur_3_2 = false;
			}
			if (!_conteneur_3_3 && __conteneur_3_3 == true) {

				View v1 = conteneur_3_3.getChildAt(0);
				conteneur_3_3.removeView(v1);

				resetImage(v1);
				v1.setVisibility(View.VISIBLE);
				test--;

				conteneur_3_3.setEnabled(true);
				v1.setEnabled(true);
				__conteneur_3_3 = false;
			}
			if (!_conteneur_3_4 && __conteneur_3_4 == true) {

				View v1 = conteneur_3_4.getChildAt(0);
				conteneur_3_4.removeView(v1);

				resetImage(v1);
				v1.setVisibility(View.VISIBLE);
				test--;

				conteneur_3_4.setEnabled(true);
				v1.setEnabled(true);
				__conteneur_3_4 = false;
			}
			if (!_conteneur_3_5 && __conteneur_3_5 == true) {

				View v1 = conteneur_3_5.getChildAt(0);
				conteneur_3_5.removeView(v1);

				resetImage(v1);
				v1.setVisibility(View.VISIBLE);
				test--;

				conteneur_3_5.setEnabled(true);
				v1.setEnabled(true);
				__conteneur_3_5 = false;
			}
			if (!_conteneur_3_6 && __conteneur_3_6 == true) {

				View v1 = conteneur_3_6.getChildAt(0);
				conteneur_3_6.removeView(v1);

				resetImage(v1);
				v1.setVisibility(View.VISIBLE);
				test--;

				conteneur_3_6.setEnabled(true);
				v1.setEnabled(true);
				__conteneur_3_6 = false;
			}

			super.handleMessage(msg);
		}

	};

	public void resetImage(View v) {
		if (v == prop_1) {
			zd_1.addView(v);
		}
		if (v == prop_2) {
			zd_2.addView(v);
		}
		if (v == prop_3) {
			zd_3.addView(v);
		}
		if (v == prop_4) {
			zd_4.addView(v);
		}
		if (v == prop_5) {
			zd_5.addView(v);
		}
		if (v == prop_6) {
			zd_6.addView(v);
		}
		if (v == prop_7) {
			zd_7.addView(v);
		}
		if (v == prop_8) {
			zd_8.addView(v);
		}
		if (v == prop_9) {
			zd_9.addView(v);
		}
		if (v == prop_10) {
			zd_10.addView(v);
		}
		if (v == prop_11) {
			zd_11.addView(v);
		}
		if (v == prop_12) {
			zd_12.addView(v);
		}
	}
}