package com.sanabilmed.science3N.M1.m2;

import android.os.Bundle;

import com.sanabilmed.science3N.R;

public class Page2_m2 extends Scene_m2{

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		imageDrawable = R.drawable.sc2_m2;
		sound = R.raw.sc2_m2;
		hasNext = false;
		nextActivity = null;
		zoomed = true;
		super.onCreate(savedInstanceState);
	}
}