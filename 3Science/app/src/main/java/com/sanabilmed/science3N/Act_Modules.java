package com.sanabilmed.science3N;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.sanabilmed.science3N.M1.m1.Module1_m1;
import com.sanabilmed.science3N.M1.m2.Module1_m2;
import com.sanabilmed.science3N.M1.m3.Module1_m3;
import com.sanabilmed.science3N.M1.m5.Module1_m5;
import com.sanabilmed.science3N.M1.m6.Module1_m6;
import com.sanabilmed.science3N.otherClasses.ImageAdapter;

public class Act_Modules extends Activity implements OnClickListener 
{
	private ImageView mod1, mod2, mod3, exit, home;
	//	private String packageName;      ramzi ♣
	//	private List<PackageInfo> packs; ramzi ♣
	private boolean scalingComplete = false;
	//	private boolean test1 = false, test2 = false, test3 = false;    ramzi ♣
	private ListView listViewArticles;

	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tr3);

		home = (ImageView) findViewById(R.id.home);
		exit = (ImageView) findViewById(R.id.exit);
		listViewArticles = (ListView) findViewById(R.id.listView1);

		exit.setOnClickListener(this);
		home.setOnClickListener(this);

		//		PackageManager pm = getPackageManager();    
		//		packs = pm.getInstalledPackages(0);

		// activate(); ramzi  



		// ramzi ♣ :  
		Integer [] data={R.drawable.module_1,R.drawable.module_2,R.drawable.module_3,R.drawable.module_1,R.drawable.module_2};
		ArrayAdapter<Integer> adapter =new ImageAdapter(Act_Modules.this, R.layout.listview_item_row, data);
		listViewArticles.setAdapter(adapter);
		listViewArticles.setDivider(null);

		listViewArticles.setOnItemClickListener(new OnItemClickListener()
		{
			public void onItemClick(AdapterView<?> parent, View view,int position, long id)
			{
				switch (position)
				{
				// ch
				case 0:
					Intent i0 = new Intent(getApplicationContext(), Module1_m1.class);
					startActivity(i0);
					finish();
					break;

				case 1:
					Intent i1 = new Intent(getApplicationContext(), Module1_m2.class);
					startActivity(i1);
					finish();
					break;

				case 2:
					Intent i2 = new Intent(getApplicationContext(), Module1_m3.class);
					startActivity(i2);
					finish();
					break;

				case 3:
					Intent i5 = new Intent(getApplicationContext(), Module1_m5.class);
					startActivity(i5);
					finish();
					break;
				case 4:
					Intent i6 = new Intent(getApplicationContext(), Module1_m6.class);
					startActivity(i6);
					finish();
					break;

				default:
					break;
				}
			}
		});
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId()) 
		{
		case R.id.exit:
			finish();
			break;
		case R.id.home:
			Intent int_home = new Intent(this, Act_home.class);
			startActivityForResult(int_home, 1000);
			overridePendingTransition(android.R.anim.fade_in,
					android.R.anim.fade_out);
			finish();
			break;

		default:
			break;
		}
	}

	@Override
	public void onBackPressed() 
	{
		// Do Here what ever you want do on back press;
	}

	//	public void activate()     ramzi
	//	{
	//
	//		for (int i = 0; i < packs.size(); i++) 
	//		{
	//			PackageInfo p = packs.get(i);
	//			packageName = p.packageName;
	//			if (packageName.equals("com.sanabilmed.math1Np1"))
	//			{
	//				test1 = true;
	//			}
	//			if (packageName.equals("com.sanabilmed.math1Np2"))
	//			{
	//				test1 = true;
	//			}
	//			if (packageName.equals("com.sanabilmed.math1Np3"))
	//			{
	//				test1 = true;
	//			}
	//			if (packageName
	//					.equals("com.sanabilmed.math_1annee_1trimestre_1module"))
	//			{
	//				test1 = true;
	//			}
	//			if (packageName
	//					.equals("com.sanabilmed.math_1annee_1trimestre_1modulep2"))
	//			{
	//				test1 = true;
	//			}
	//			if (packageName
	//					.equals("com.sanabilmed.math_1annee_1trimestre_1modulep3")) 
	//			{
	//				test1 = true;
	//			}
	//			if (packageName
	//					.equals("com.sanabilmed.math_1annee_3trimestre_1module"))
	//			{
	//				test1 = true;
	//			}
	//			if (packageName
	//					.equals("com.sanabilmed.math_1annee_3trimestre_1modulep2"))
	//			{
	//				test1 = true;
	//			}
	//
	//			if (packageName
	//					.equals("com.sanabilmed.math_1annee_3trimestre_1modulep3"))
	//			{
	//				test2 = true;
	//			}
	//
	//		}
	//
	//	}









	//////////////////////////////////////////////////////////////////////////////////////////////////

	public void onWindowFocusChanged(boolean hasFocus) 
	{
		if (!scalingComplete) // only do this once
		{
			scaleContents(findViewById(R.id.contents),
					findViewById(R.id.container));
			scalingComplete = true;
		}
		super.onWindowFocusChanged(hasFocus);
	}

	public View onCreateView(String name, Context context, AttributeSet attrs)
	{
		View view = super.onCreateView(name, context, attrs);
		return view;
	}

	private void scaleContents(View rootView, View container) 
	{
		float xScale = (float) container.getWidth() / rootView.getWidth();
		float yScale = (float) container.getHeight() / rootView.getHeight();
		float scale = Math.min(xScale, yScale);

		scaleViewAndChildren(rootView, scale);
	}

	public static void scaleViewAndChildren(View root, float scale)
	{
		ViewGroup.LayoutParams layoutParams = root.getLayoutParams();

		if (layoutParams.width != ViewGroup.LayoutParams.FILL_PARENT
				&& layoutParams.width != ViewGroup.LayoutParams.WRAP_CONTENT)
		{
			layoutParams.width *= scale;
		}
		if (layoutParams.height != ViewGroup.LayoutParams.FILL_PARENT
				&& layoutParams.height != ViewGroup.LayoutParams.WRAP_CONTENT)
		{
			layoutParams.height *= scale;
		}

		if (layoutParams instanceof ViewGroup.MarginLayoutParams) 
		{
			ViewGroup.MarginLayoutParams marginParams = (ViewGroup.MarginLayoutParams) layoutParams;
			marginParams.leftMargin *= scale;
			marginParams.rightMargin *= scale;
			marginParams.topMargin *= scale;
			marginParams.bottomMargin *= scale;
		}

		root.setLayoutParams(layoutParams);

		root.setPadding((int) (root.getPaddingLeft() * scale),
				(int) (root.getPaddingTop() * scale),
				(int) (root.getPaddingRight() * scale),
				(int) (root.getPaddingBottom() * scale));

		if (root instanceof TextView) 
		{
			TextView textView = (TextView) root;
			textView.setTextSize(textView.getTextSize() * scale);

		}

		if (root instanceof ViewGroup)
		{
			ViewGroup groupView = (ViewGroup) root;
			for (int cnt = 0; cnt < groupView.getChildCount(); ++cnt)
				scaleViewAndChildren(groupView.getChildAt(cnt), scale);
		}
	}



}
