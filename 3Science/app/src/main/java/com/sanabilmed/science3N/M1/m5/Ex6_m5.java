package com.sanabilmed.science3N.M1.m5;

import java.util.Random;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager.WakeLock;
import android.util.AttributeSet;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.DragShadowBuilder;
import android.view.View.OnClickListener;
import android.view.View.OnDragListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sanabilmed.science3N.Act_Modules;
import com.sanabilmed.science3N.R;


public class Ex6_m5 extends Activity 
{
	ImageView obj_1, obj_2, obj_3, obj_4, obj_5;
	LinearLayout za1, za2, za3, za4, za5;
	LinearLayout zd1, zd2, zd3, zd4, zd5;
	private WakeLock mWakeLock;
	private ImageView rep, ex3,ex1, ex2, ex4, ex5, audio,
	imgVer, imgCorr, imgVerEx2, img1, img2, img3, img4, exit, home;
	private MediaPlayer mp;
	private ImageView rep1, rep2, rep3, rep4, rep5, rep6, rep7, rep8;

	AnimationSet animation;
	boolean _rep1 = false, _rep2 = false, _rep3 = false, _rep4 = false,
			_rep5 = false, _rep6 = false, _rep7 = false, _rep8 = false, _rep9 = false, _rep10 = false, _rep11 = false, _rep12 = false;
	private boolean scalingComplete = false;
	Activity act;
	ClipData data;
	int randomInt;

	private static final long active = 2000;
	private static final int STOP = 0;
	private Message msg;
	private ImageView prop_1, prop_2, prop_3, prop_4, prop_5, prop_6, prop_7;

	private RelativeLayout conteneur_1_1, conteneur_1_2, conteneur_1_3,
	conteneur_1_4;
	private RelativeLayout conteneur_2_1, conteneur_2_2, conteneur_2_3,
	conteneur_2_4;

	//     _conteneur
	private Boolean _conteneur_1_1 = false, _conteneur_1_2 = false,
			_conteneur_1_3 = false, _conteneur_1_4 = false;
	private Boolean _conteneur_2_1 = false, _conteneur_2_2 = false,
			_conteneur_2_3 = false, _conteneur_2_4 = false;
	//     __conteneur
	private Boolean __conteneur_1_1 = false, __conteneur_1_2 = false,
			__conteneur_1_3 = false, __conteneur_1_4 = false;
	private Boolean __conteneur_2_1 = false, __conteneur_2_2 = false,
			__conteneur_2_3 = false, __conteneur_2_4 = false;
	int test = 0;
	private RelativeLayout zd_1, zd_2, zd_3, zd_4, zd_5, zd_6,zd_7;
	int cont_1 = 0, cont_2 = 0;

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public void onCreate(Bundle savedInstanceState)
	{
		setContentView(R.layout.ex6_m5);
		exit = (ImageView) findViewById(R.id.exit);
		home = (ImageView) findViewById(R.id.home);

		act = this;

		img2 = (ImageView) findViewById(R.id.b2);

		ex1 = (ImageView) findViewById(R.id.ex_1);
		ex2 = (ImageView) findViewById(R.id.ex_2);
		ex3 = (ImageView) findViewById(R.id.ex_3);
		ex4 = (ImageView) findViewById(R.id.ex_4);
		ex5 = (ImageView) findViewById(R.id.ex_5);

		imgCorr = (ImageView) findViewById(R.id.imgCorr);
		imgVer = (ImageView) findViewById(R.id.imgVer);
		rep = (ImageView) findViewById(R.id.repeat);
		audio = (ImageView) findViewById(R.id.jouerSon);

		// ////////////////////////////fin gestion
		// mp////////////////////////////////

		prop_1 = (ImageView) findViewById(R.id.obj_1);
		prop_2 = (ImageView) findViewById(R.id.obj_2);
		prop_3 = (ImageView) findViewById(R.id.obj_3);
		prop_4 = (ImageView) findViewById(R.id.obj_4);
		prop_5 = (ImageView) findViewById(R.id.obj_5);
		prop_6 = (ImageView) findViewById(R.id.obj_6);
		prop_7 = (ImageView) findViewById(R.id.obj_7);

		zd_1 = (RelativeLayout) findViewById(R.id.zd_1);
		zd_2 = (RelativeLayout) findViewById(R.id.zd_2);
		zd_3 = (RelativeLayout) findViewById(R.id.zd_3);
		zd_4 = (RelativeLayout) findViewById(R.id.zd_4);
		zd_5 = (RelativeLayout) findViewById(R.id.zd_5);
		zd_6 = (RelativeLayout) findViewById(R.id.zd_6);
		zd_7 = (RelativeLayout) findViewById(R.id.zd_7);

		conteneur_1_1 = (RelativeLayout) findViewById(R.id.conteneur_1_1);
		conteneur_1_2 = (RelativeLayout) findViewById(R.id.conteneur_1_2);
		conteneur_1_3 = (RelativeLayout) findViewById(R.id.conteneur_1_3);
		conteneur_1_4 = (RelativeLayout) findViewById(R.id.conteneur_1_4);

		conteneur_2_1 = (RelativeLayout) findViewById(R.id.conteneur_2_1);
		conteneur_2_2 = (RelativeLayout) findViewById(R.id.conteneur_2_2);
		conteneur_2_3 = (RelativeLayout) findViewById(R.id.conteneur_2_3);
		conteneur_2_4 = (RelativeLayout) findViewById(R.id.conteneur_2_4);

		Animation fadeIn = new AlphaAnimation(0, 1);
		fadeIn.setInterpolator(new DecelerateInterpolator()); // add this
		fadeIn.setDuration(1000);

		Animation fadeOut = new AlphaAnimation(1, 0);
		fadeOut.setInterpolator(new AccelerateInterpolator()); // and this
		fadeOut.setStartOffset(2500);// 3500
		fadeOut.setDuration(1000);

		animation = new AnimationSet(true); // change to false
		animation.addAnimation(fadeIn);
		animation.addAnimation(fadeOut);

		super.onCreate(savedInstanceState);

		prop_1.setOnTouchListener(new OnTouchListener() {

			public boolean onTouch(View view, MotionEvent motionEvent)
			{
				if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) 
				{
					DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
					view.startDrag(data, shadowBuilder, view, 0);
					view.setVisibility(View.INVISIBLE);

					return true;
				}

				else 
				{
					prop_1.setVisibility(View.VISIBLE);
					return false;
				}
			}
		});
		prop_2.setOnTouchListener(new OnTouchListener() {

			public boolean onTouch(View view, MotionEvent motionEvent) {

				if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
					DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(
							view);

					view.startDrag(data, shadowBuilder, view, 0);
					view.setVisibility(View.INVISIBLE);

					return true;
				}

				else {
					prop_2.setVisibility(View.VISIBLE);
					return false;
				}
			}
		});
		prop_3.setOnTouchListener(new OnTouchListener() {

			public boolean onTouch(View view, MotionEvent motionEvent) {

				if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
					DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(
							view);

					view.startDrag(data, shadowBuilder, view, 0);
					view.setVisibility(View.INVISIBLE);

					return true;
				}

				else {
					prop_3.setVisibility(View.VISIBLE);
					return false;
				}
			}
		});
		prop_4.setOnTouchListener(new OnTouchListener() {

			public boolean onTouch(View view, MotionEvent motionEvent) {

				if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
					DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(
							view);

					view.startDrag(data, shadowBuilder, view, 0);
					view.setVisibility(View.INVISIBLE);

					return true;
				}

				else {
					prop_4.setVisibility(View.VISIBLE);
					return false;
				}
			}
		});
		prop_5.setOnTouchListener(new OnTouchListener() {

			public boolean onTouch(View view, MotionEvent motionEvent) {

				if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
					DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(
							view);

					view.startDrag(data, shadowBuilder, view, 0);
					view.setVisibility(View.INVISIBLE);

					return true;
				}

				else {
					prop_5.setVisibility(View.VISIBLE);
					return false;
				}
			}
		});
		prop_6.setOnTouchListener(new OnTouchListener() {

			public boolean onTouch(View view, MotionEvent motionEvent) {

				if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
					DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(
							view);

					view.startDrag(data, shadowBuilder, view, 0);
					view.setVisibility(View.INVISIBLE);

					return true;
				}

				else {
					prop_6.setVisibility(View.VISIBLE);
					return false;
				}
			}
		});
		prop_7.setOnTouchListener(new OnTouchListener() {

			public boolean onTouch(View view, MotionEvent motionEvent) {

				if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
					DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(
							view);

					view.startDrag(data, shadowBuilder, view, 0);
					view.setVisibility(View.INVISIBLE);

					return true;
				}

				else {
					prop_7.setVisibility(View.VISIBLE);
					return false;
				}
			}
		});



		conteneur_1_1.setOnDragListener(new OnDragListener() {

			public boolean onDrag(View v, DragEvent event) {
				switch (event.getAction()) {
				case DragEvent.ACTION_DRAG_STARTED:
					break;
				case DragEvent.ACTION_DRAG_ENTERED:
					break;
				case DragEvent.ACTION_DRAG_EXITED:
					break;
				case DragEvent.ACTION_DROP:
					View view4 = (View) event.getLocalState();
					ViewGroup owner = (ViewGroup) view4.getParent();
					owner.removeView(view4);
					RelativeLayout container = (RelativeLayout) v;
					container.addView(view4);
					view4.setVisibility(View.VISIBLE);
					conteneur_1_1.setEnabled(false);
					view4.setEnabled(false);
					if (view4 == prop_1 || view4 == prop_3 || view4 == prop_6 ) 
					{
						_conteneur_1_1 = true;
						cont_1++;
					}
					test++;
					__conteneur_1_1 = true;
					break;
				case DragEvent.ACTION_DRAG_ENDED:
				default:
					break;
				}
				return true;
			}
		});

		conteneur_1_2.setOnDragListener(new OnDragListener() {

			public boolean onDrag(View v, DragEvent event) {
				switch (event.getAction()) {
				case DragEvent.ACTION_DRAG_STARTED:
					break;
				case DragEvent.ACTION_DRAG_ENTERED:
					break;
				case DragEvent.ACTION_DRAG_EXITED:
					break;
				case DragEvent.ACTION_DROP:
					View view4 = (View) event.getLocalState();
					ViewGroup owner = (ViewGroup) view4.getParent();
					owner.removeView(view4);
					RelativeLayout container = (RelativeLayout) v;
					container.addView(view4);
					conteneur_1_2.setEnabled(false);
					view4.setVisibility(View.VISIBLE);
					view4.setEnabled(false);
					if (view4 == prop_1 || view4 == prop_3 || view4 == prop_6 ) {
						_conteneur_1_2 = true;
						cont_1++;
					}
					test++;
					__conteneur_1_2 = true;
					break;
				case DragEvent.ACTION_DRAG_ENDED:
				default:
					break;
				}
				return true;
			}
		});
		conteneur_1_3.setOnDragListener(new OnDragListener() {

			public boolean onDrag(View v, DragEvent event) {
				switch (event.getAction()) {
				case DragEvent.ACTION_DRAG_STARTED:
					break;
				case DragEvent.ACTION_DRAG_ENTERED:
					break;
				case DragEvent.ACTION_DRAG_EXITED:
					break;
				case DragEvent.ACTION_DROP:
					View view4 = (View) event.getLocalState();
					ViewGroup owner = (ViewGroup) view4.getParent();
					owner.removeView(view4);
					RelativeLayout container = (RelativeLayout) v;
					container.addView(view4);
					view4.setVisibility(View.VISIBLE);
					view4.setEnabled(false);
					conteneur_1_3.setEnabled(false);
					if (view4 == prop_1 || view4 == prop_3 || view4 == prop_6 ) {
						_conteneur_1_3 = true;
						cont_1++;
					}
					__conteneur_1_3 = true;
					test++;

					break;
				case DragEvent.ACTION_DRAG_ENDED:
				default:
					break;
				}
				return true;
			}
		});
		conteneur_1_4.setOnDragListener(new OnDragListener() {

			public boolean onDrag(View v, DragEvent event) {
				switch (event.getAction()) {
				case DragEvent.ACTION_DRAG_STARTED:
					break;
				case DragEvent.ACTION_DRAG_ENTERED:
					break;
				case DragEvent.ACTION_DRAG_EXITED:
					break;
				case DragEvent.ACTION_DROP:
					View view4 = (View) event.getLocalState();
					ViewGroup owner = (ViewGroup) view4.getParent();
					owner.removeView(view4);
					RelativeLayout container = (RelativeLayout) v;
					container.addView(view4);
					view4.setVisibility(View.VISIBLE);
					view4.setEnabled(false);
					conteneur_1_4.setEnabled(false);
					if (view4 == prop_1 || view4 == prop_3 || view4 == prop_6 ) {
						_conteneur_1_4 = true;
						cont_1++;
					}
					__conteneur_1_4 = true;
					test++;

					break;
				case DragEvent.ACTION_DRAG_ENDED:
				default:
					break;
				}
				return true;
			}
		});


		conteneur_2_1.setOnDragListener(new OnDragListener() {

			public boolean onDrag(View v, DragEvent event) {
				switch (event.getAction()) {
				case DragEvent.ACTION_DRAG_STARTED:
					break;
				case DragEvent.ACTION_DRAG_ENTERED:
					break;
				case DragEvent.ACTION_DRAG_EXITED:
					break;
				case DragEvent.ACTION_DROP:
					View view4 = (View) event.getLocalState();
					ViewGroup owner = (ViewGroup) view4.getParent();
					owner.removeView(view4);
					RelativeLayout container = (RelativeLayout) v;
					container.addView(view4);
					view4.setVisibility(View.VISIBLE);
					view4.setEnabled(false);
					conteneur_2_1.setEnabled(false);
					if (view4 == prop_2 || view4 == prop_4 || view4 == prop_5|| view4 == prop_7) {
						_conteneur_2_1 = true;
						cont_2++;
					}
					__conteneur_2_1 = true;
					test++;

					break;
				case DragEvent.ACTION_DRAG_ENDED:
				default:
					break;
				}
				return true;
			}
		});
		conteneur_2_2.setOnDragListener(new OnDragListener() {

			public boolean onDrag(View v, DragEvent event) {
				switch (event.getAction()) {
				case DragEvent.ACTION_DRAG_STARTED:
					break;
				case DragEvent.ACTION_DRAG_ENTERED:
					break;
				case DragEvent.ACTION_DRAG_EXITED:
					break;
				case DragEvent.ACTION_DROP:
					View view4 = (View) event.getLocalState();
					ViewGroup owner = (ViewGroup) view4.getParent();
					owner.removeView(view4);
					RelativeLayout container = (RelativeLayout) v;
					container.addView(view4);
					view4.setVisibility(View.VISIBLE);
					view4.setEnabled(false);
					conteneur_2_2.setEnabled(false);
					if (view4 == prop_2 || view4 == prop_4 || view4 == prop_5|| view4 == prop_7) {
						_conteneur_2_2 = true;
						cont_2++;
					}
					test++;
					__conteneur_2_2 = true;

					break;
				case DragEvent.ACTION_DRAG_ENDED:
				default:
					break;
				}
				return true;
			}
		});
		conteneur_2_3.setOnDragListener(new OnDragListener() {

			public boolean onDrag(View v, DragEvent event) {
				switch (event.getAction()) {
				case DragEvent.ACTION_DRAG_STARTED:
					break;
				case DragEvent.ACTION_DRAG_ENTERED:
					break;
				case DragEvent.ACTION_DRAG_EXITED:
					break;
				case DragEvent.ACTION_DROP:
					View view4 = (View) event.getLocalState();
					ViewGroup owner = (ViewGroup) view4.getParent();
					owner.removeView(view4);
					RelativeLayout container = (RelativeLayout) v;
					container.addView(view4);
					view4.setVisibility(View.VISIBLE);
					view4.setEnabled(false);
					conteneur_2_3.setEnabled(false);
					if (view4 == prop_2 || view4 == prop_4 || view4 == prop_5|| view4 == prop_7) {
						_conteneur_2_3 = true;
						cont_2++;
					}
					test++;
					__conteneur_2_3 = true;

					break;
				case DragEvent.ACTION_DRAG_ENDED:
				default:
					break;
				}
				return true;
			}
		});
		conteneur_2_4.setOnDragListener(new OnDragListener() {

			public boolean onDrag(View v, DragEvent event) {
				switch (event.getAction()) {
				case DragEvent.ACTION_DRAG_STARTED:
					break;
				case DragEvent.ACTION_DRAG_ENTERED:
					break;
				case DragEvent.ACTION_DRAG_EXITED:
					break;
				case DragEvent.ACTION_DROP:
					View view4 = (View) event.getLocalState();
					ViewGroup owner = (ViewGroup) view4.getParent();
					owner.removeView(view4);
					RelativeLayout container = (RelativeLayout) v;
					container.addView(view4);
					view4.setVisibility(View.VISIBLE);
					view4.setEnabled(false);
					conteneur_2_4.setEnabled(false);
					if (view4 == prop_2 || view4 == prop_4 || view4 == prop_5|| view4 == prop_7) {
						_conteneur_2_4 = true;
						cont_2++;
					}
					test++;
					__conteneur_2_4 = true;

					break;
				case DragEvent.ACTION_DRAG_ENDED:
				default:
					break;
				}
				return true;
			}
		});

		imgVer.setOnClickListener(new OnClickListener()
		{
			public void onClick(View v)
			{
				Random rand = new Random();
				randomInt = rand.nextInt();
				imgCorr.setBackgroundDrawable(null);
				imgCorr.setBackgroundDrawable(null);
				imgCorr.setBackgroundDrawable(null);
				try_stop_mp(mp);

				if (test < 7) 
				{
					imgCorr.setVisibility(1);
					imgCorr.setBackgroundResource(R.drawable.mascotte_incomplet);
					imgCorr.startAnimation(animation);
					imgCorr.setVisibility(View.INVISIBLE);
					mp = MediaPlayer.create(getApplicationContext(),R.raw.mascotte_incomplet);
					mp.start();
				} else if (cont_1 == 3 && cont_2 == 4) 
				{
					ChangerColorImage_Vert();
					imgCorr.setVisibility(1);
					imgCorr.setBackgroundResource(R.drawable.mascotte_vrai_1);
					imgCorr.startAnimation(animation);
					imgCorr.setVisibility(View.INVISIBLE);
					mp = MediaPlayer.create(getApplicationContext(),
							R.raw.mascotte_vrai_1);
					mp.start();

				} else 
				{
					ChangerColorImage_Vert();
					imgCorr.setVisibility(1);
					imgCorr.setBackgroundResource(R.drawable.mascotte_faux_2);
					imgCorr.startAnimation(animation);
					imgCorr.setVisibility(View.INVISIBLE);

					mp = MediaPlayer.create(getApplicationContext(),R.raw.mascotte_faux_2);
					mp.start();

					Message msg = new Message();
					msg.what = STOP;
					Handler.sendMessageDelayed(msg, active);
				}


			}
		});

		img2.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				try_stop_mp(mp);
				Intent i2 = new Intent(getApplicationContext(), Page1_m5.class);
				startActivity(i2);
				act.finish();
			}
		});

		home.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				try_stop_mp(mp);
				startActivity(new Intent(getApplicationContext(), Act_Modules.class));

				act.finish();

			}
		});
		exit.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				try_stop_mp(mp);
				finish();

			}
		});
		ex1.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				try_stop_mp(mp);
				startActivity(new Intent(getApplicationContext(), Ex1_m5.class));

				act.finish();

			}
		});

		ex2.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				try_stop_mp(mp);
				startActivity(new Intent(getApplicationContext(), Ex2_m5.class));

				act.finish();

			}
		});

		ex3.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				try_stop_mp(mp);
				startActivity(new Intent(getApplicationContext(), Ex3_m5.class));

				act.finish();

			}
		});
		ex4.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				try_stop_mp(mp);
				startActivity(new Intent(getApplicationContext(), Ex4_m5.class));

				act.finish();
			}
		});


		ex5.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				try_stop_mp(mp);
				startActivity(new Intent(getApplicationContext(), Ex5_m5.class));

				act.finish();
			}
		});




		imgCorr.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				imgCorr.setVisibility(View.INVISIBLE);

			}
		});

		rep.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				try_stop_mp(mp);
				finish();
				startActivity(getIntent());
			}
		});

		audio.setOnClickListener(new OnClickListener() 
		{
			public void onClick(View v)
			{
				try_stop_mp(mp);
				mp = new MediaPlayer();
				mp = MediaPlayer.create(Ex6_m5.this, R.raw.e3_m5_ex6);
				mp.start();
			}
		});

	}
	// ramzi ♣ :
	public void ChangerViewContenair(RelativeLayout contenaire)
	{
		View v1 = contenaire.getChildAt(0); 
		if (v1==prop_1)
		{
			contenaire.removeView(prop_1);
			prop_1.setImageResource(R.drawable.e3_m5_ex6_ic4_faux);
			contenaire.addView(prop_1);
		}
		if (v1==prop_2)
		{
			contenaire.removeView(prop_2);
			prop_2.setImageResource(R.drawable.e3_m5_ex6_ic3_faux);
			contenaire.addView(prop_2);
		}if (v1==prop_3)
		{
			contenaire.removeView(prop_3);
			prop_3.setImageResource(R.drawable.e3_m5_ex6_ic2_faux);
			contenaire.addView(prop_3);
		}
		if (v1==prop_4)
		{
			contenaire.removeView(prop_4);
			prop_4.setImageResource(R.drawable.e3_m5_ex6_ic1_faux);
			contenaire.addView(prop_4);
		}if (v1==prop_5)
		{
			contenaire.removeView(prop_5);
			prop_5.setImageResource(R.drawable.e3_m5_ex6_ic7_faux);
			contenaire.addView(prop_5);
		}
		if (v1==prop_6)
		{
			contenaire.removeView(prop_6);
			prop_6.setImageResource(R.drawable.e3_m5_ex6_ic6_faux);
			contenaire.addView(prop_6);
		}
		if (v1==prop_7)
		{
			contenaire.removeView(prop_7);
			prop_7.setImageResource(R.drawable.e3_m5_ex6_ic5_faux);
			contenaire.addView(prop_7);
		}
	}
	// ramzi  ☺:
	public void ChangerColorImage_Vert()
	{
		// conteneur 1
		if (_conteneur_1_1==true)
		{
			View v1 = conteneur_1_1.getChildAt(0);
			if (v1==prop_1) 
			{
				conteneur_1_1.setBackgroundResource(R.drawable.e3_m5_ex6_ic4_vrai);
			}
			if (v1==prop_3) 
			{
				conteneur_1_1.setBackgroundResource(R.drawable.e3_m5_ex6_ic2_vrai);
			}
			if (v1==prop_6) 
			{
				conteneur_1_1.setBackgroundResource(R.drawable.e3_m5_ex6_ic6_vrai);
			}
			conteneur_1_1.removeView(v1);
		}else
		{
			ChangerViewContenair(conteneur_1_1);
		}
		if (_conteneur_1_2==true)
		{
			View v2 = conteneur_1_2.getChildAt(0);
			if (v2==prop_1) 
			{
				conteneur_1_2.setBackgroundResource(R.drawable.e3_m5_ex6_ic4_vrai);
			}
			if (v2==prop_3) 
			{
				conteneur_1_2.setBackgroundResource(R.drawable.e3_m5_ex6_ic2_vrai);
			}
			if (v2==prop_6) 
			{
				conteneur_1_2.setBackgroundResource(R.drawable.e3_m5_ex6_ic6_vrai);
			}
			conteneur_1_2.removeView(v2);
		}else
		{
			ChangerViewContenair(conteneur_1_2);

		}
		if (_conteneur_1_3==true)
		{
			View v3 = conteneur_1_3.getChildAt(0);
			if (v3==prop_1) 
			{
				conteneur_1_3.setBackgroundResource(R.drawable.e3_m5_ex6_ic4_vrai);
			}
			if (v3==prop_3) 
			{
				conteneur_1_3.setBackgroundResource(R.drawable.e3_m5_ex6_ic2_vrai);
			}
			if (v3==prop_6) 
			{
				conteneur_1_3.setBackgroundResource(R.drawable.e3_m5_ex6_ic6_vrai);
			}
			conteneur_1_3.removeView(v3);
		}else
		{
			ChangerViewContenair(conteneur_1_3);
		}
		if (_conteneur_1_4==true)
		{
			View v4 = conteneur_1_4.getChildAt(0);
			if (v4==prop_1) 
			{
				conteneur_1_4.setBackgroundResource(R.drawable.e3_m5_ex6_ic4_vrai);
			}
			if (v4==prop_3) 
			{
				conteneur_1_4.setBackgroundResource(R.drawable.e3_m5_ex6_ic2_vrai);
			}
			if (v4==prop_6) 
			{
				conteneur_1_4.setBackgroundResource(R.drawable.e3_m5_ex6_ic6_vrai);
			}
			conteneur_1_4.removeView(v4);
		}else
		{
			ChangerViewContenair(conteneur_1_4);
		}
		// conteneur 2
		if (_conteneur_2_1==true)
		{
			View v1 = conteneur_2_1.getChildAt(0);
			if (v1==prop_2) 
			{
				conteneur_2_1.setBackgroundResource(R.drawable.e3_m5_ex6_ic3_vrai);
			}
			if (v1==prop_4) 
			{
				conteneur_2_1.setBackgroundResource(R.drawable.e3_m5_ex6_ic1_vrai);
			}
			if (v1==prop_5) 
			{
				conteneur_2_1.setBackgroundResource(R.drawable.e3_m5_ex6_ic7_vrai);
			}
			if (v1==prop_7) 
			{
				conteneur_2_1.setBackgroundResource(R.drawable.e3_m5_ex6_ic5_vrai);
			}
			conteneur_2_1.removeView(v1);
		}else
		{
			ChangerViewContenair(conteneur_2_1);	
		}
		if (_conteneur_2_2==true)
		{
			View v2 = conteneur_2_2.getChildAt(0);
			if (v2==prop_2) 
			{
				conteneur_2_2.setBackgroundResource(R.drawable.e3_m5_ex6_ic3_vrai);
			}
			if (v2==prop_4) 
			{
				conteneur_2_2.setBackgroundResource(R.drawable.e3_m5_ex6_ic1_vrai);
			}
			if (v2==prop_5) 
			{
				conteneur_2_2.setBackgroundResource(R.drawable.e3_m5_ex6_ic7_vrai);
			}
			if (v2==prop_7) 
			{
				conteneur_2_2.setBackgroundResource(R.drawable.e3_m5_ex6_ic5_vrai);
			}
			conteneur_2_2.removeView(v2);
		}else
		{
			ChangerViewContenair(conteneur_2_2);	
		}
		if (_conteneur_2_3==true)
		{
			View v3 = conteneur_2_3.getChildAt(0);
			if (v3==prop_2) 
			{
				conteneur_2_3.setBackgroundResource(R.drawable.e3_m5_ex6_ic3_vrai);
			}
			if (v3==prop_4) 
			{
				conteneur_2_3.setBackgroundResource(R.drawable.e3_m5_ex6_ic1_vrai);
			}
			if (v3==prop_5) 
			{
				conteneur_2_3.setBackgroundResource(R.drawable.e3_m5_ex6_ic7_vrai);
			}
			if (v3==prop_7) 
			{
				conteneur_2_3.setBackgroundResource(R.drawable.e3_m5_ex6_ic5_vrai);
			}
			conteneur_2_3.removeView(v3);
		}else
		{
			ChangerViewContenair(conteneur_2_3);
		}
		if (_conteneur_2_4==true)
		{
			View v4 = conteneur_2_4.getChildAt(0);
			if (v4==prop_2) 
			{
				conteneur_2_4.setBackgroundResource(R.drawable.e3_m5_ex6_ic3_vrai);
			}
			if (v4==prop_4) 
			{
				conteneur_2_4.setBackgroundResource(R.drawable.e3_m5_ex6_ic1_vrai);
			}
			if (v4==prop_5) 
			{
				conteneur_2_4.setBackgroundResource(R.drawable.e3_m5_ex6_ic7_vrai);
			}
			if (v4==prop_7) 
			{
				conteneur_2_4.setBackgroundResource(R.drawable.e3_m5_ex6_ic5_vrai);
			}
			conteneur_2_4.removeView(v4);
		}else
		{
			ChangerViewContenair(conteneur_2_4);
		}
	}
	private Handler Handler = new Handler() 
	{
		public void handleMessage(Message msg) 
		{

			if (!_conteneur_1_1 && __conteneur_1_1 == true) 
			{
				View v1 = conteneur_1_1.getChildAt(0);
				conteneur_1_1.removeView(v1);
				resetImage(v1);
				v1.setVisibility(View.VISIBLE);
				test--;

				conteneur_1_1.setEnabled(true);
				v1.setEnabled(true);
				__conteneur_1_1 = false;
			}

			if (!_conteneur_1_2 && __conteneur_1_2 == true) {

				View v1 = conteneur_1_2.getChildAt(0);
				conteneur_1_2.removeView(v1);

				resetImage(v1);
				v1.setVisibility(View.VISIBLE);
				test--;

				conteneur_1_2.setEnabled(true);
				v1.setEnabled(true);
				__conteneur_1_2 = false;
			}

			if (!_conteneur_1_3 && __conteneur_1_3 == true) {

				View v1 = conteneur_1_3.getChildAt(0);
				conteneur_1_3.removeView(v1);

				resetImage(v1);
				v1.setVisibility(View.VISIBLE);
				test--;

				conteneur_1_3.setEnabled(true);
				v1.setEnabled(true);
				__conteneur_1_3 = false;
			}

			if (!_conteneur_1_4 && __conteneur_1_4 == true) {

				View v1 = conteneur_1_4.getChildAt(0);
				conteneur_1_4.removeView(v1);

				resetImage(v1);
				v1.setVisibility(View.VISIBLE);
				test--;

				conteneur_1_4.setEnabled(true);
				v1.setEnabled(true);
				__conteneur_1_4 = false;
			}


			// 2

			if (!_conteneur_2_1 && __conteneur_2_1 == true) {

				View v1 = conteneur_2_1.getChildAt(0);
				conteneur_2_1.removeView(v1);

				resetImage(v1);
				v1.setVisibility(View.VISIBLE);
				test--;

				conteneur_2_1.setEnabled(true);
				v1.setEnabled(true);
				__conteneur_2_1 = false;
			}

			if (!_conteneur_2_2 && __conteneur_2_2 == true) {

				View v1 = conteneur_2_2.getChildAt(0);
				conteneur_2_2.removeView(v1);

				resetImage(v1);
				v1.setVisibility(View.VISIBLE);
				test--;

				conteneur_2_2.setEnabled(true);
				v1.setEnabled(true);
				__conteneur_2_2 = false;
			}

			if (!_conteneur_2_3 && __conteneur_2_3 == true) {

				View v1 = conteneur_2_3.getChildAt(0);
				conteneur_2_3.removeView(v1);

				resetImage(v1);
				v1.setVisibility(View.VISIBLE);
				test--;

				conteneur_2_3.setEnabled(true);
				v1.setEnabled(true);
				__conteneur_2_3 = false;
			}

			if (!_conteneur_2_4 && __conteneur_2_4 == true) {

				View v1 = conteneur_2_4.getChildAt(0);
				conteneur_2_4.removeView(v1);

				resetImage(v1);
				v1.setVisibility(View.VISIBLE);
				test--;

				conteneur_2_4.setEnabled(true);
				v1.setEnabled(true);
				__conteneur_2_4 = false;
			}


			super.handleMessage(msg);
		}

	};
	// ramzi ☺ :
	public void resetImage(View v) 
	{
		if (v == prop_1)
		{
			prop_1.setImageResource(R.drawable.e3_m5_ex6_ic4_select);
			zd_1.addView(prop_1);
		}
		if (v == prop_2)
		{
			prop_2.setImageResource(R.drawable.e3_m5_ex6_ic3_select);
			zd_2.addView(prop_2);
		}
		if (v == prop_3)
		{
			prop_3.setImageResource(R.drawable.e3_m5_ex6_ic2_select);
			zd_3.addView(prop_3);
		}
		if (v == prop_4)
		{
			prop_4.setImageResource(R.drawable.e3_m5_ex6_ic1_select);
			zd_4.addView(prop_4);
		}
		if (v == prop_5)
		{
			prop_5.setImageResource(R.drawable.e3_m5_ex6_ic7_select);
			zd_5.addView(prop_5);
		}
		if (v == prop_6)
		{
			prop_6.setImageResource(R.drawable.e3_m5_ex6_ic6_select);
			zd_6.addView(prop_6);
		}
		if (v == prop_7)
		{
			prop_7.setImageResource(R.drawable.e3_m5_ex6_ic5_select);
			zd_7.addView(prop_7);
		}
	}
	public void try_stop_mp(MediaPlayer mp) 
	{
		try {
			if (mp != null) 
			{
				mp.stop();
				mp.release();
			}

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	// protected void onDestroy() {
	// mp.release();
	// mWakeLock.release();
	// super.onDestroy();
	// }

	protected void onRestart() {
		super.onRestart();
	}

	protected void onPause() {
		act.finish();
		super.onPause();
	}
	public boolean isOnline() {

		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}

	// ///////////////////////////////////////////////////////////////////////////////////////////
	public void onWindowFocusChanged(boolean hasFocus) {
		if (!scalingComplete) // only do this once
		{
			scaleContents(findViewById(R.id.contents),
					findViewById(R.id.container));
			scalingComplete = true;
		}
		super.onWindowFocusChanged(hasFocus);
	}

	/**
	 * Called when the views have been created. We override this in order to
	 * scale the UI, which we can't do before this.
	 */
	@Override
	public View onCreateView(String name, Context context, AttributeSet attrs) {
		View view = super.onCreateView(name, context, attrs);
		return view;
	}

	private void scaleContents(View rootView, View container) {
		float xScale = (float) container.getWidth() / rootView.getWidth();
		float yScale = (float) container.getHeight() / rootView.getHeight();
		float scale = Math.min(xScale, yScale);
		scaleViewAndChildren(rootView, scale);
	}

	public static void scaleViewAndChildren(View root, float scale) {
		ViewGroup.LayoutParams layoutParams = root.getLayoutParams();

		// Scale the view itself
		if (layoutParams.width != ViewGroup.LayoutParams.FILL_PARENT
				&& layoutParams.width != ViewGroup.LayoutParams.WRAP_CONTENT) {
			layoutParams.width *= scale;
		}
		if (layoutParams.height != ViewGroup.LayoutParams.FILL_PARENT
				&& layoutParams.height != ViewGroup.LayoutParams.WRAP_CONTENT) {
			layoutParams.height *= scale;
		}

		if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
			ViewGroup.MarginLayoutParams marginParams = (ViewGroup.MarginLayoutParams) layoutParams;
			marginParams.leftMargin *= scale;
			marginParams.rightMargin *= scale;
			marginParams.topMargin *= scale;
			marginParams.bottomMargin *= scale;
		}

		root.setLayoutParams(layoutParams);

		root.setPadding((int) (root.getPaddingLeft() * scale),
				(int) (root.getPaddingTop() * scale),
				(int) (root.getPaddingRight() * scale),
				(int) (root.getPaddingBottom() * scale));

		if (root instanceof TextView) {
			TextView textView = (TextView) root;
			textView.setTextSize(textView.getTextSize() * scale);
		}

		if (root instanceof ViewGroup) {
			ViewGroup groupView = (ViewGroup) root;
			for (int cnt = 0; cnt < groupView.getChildCount(); ++cnt)
				scaleViewAndChildren(groupView.getChildAt(cnt), scale);
		}
	}



}