package com.sanabilmed.science3N.otherClasses;

import android.app.Activity;
import android.graphics.Matrix;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;

public class AnimHh {

	private static float MARGE = 1.0f;
	private  boolean StopedThread;
	private static int X_SCALE, Y_SCALE, X_TRANS, Y_TRANS, counter;
	private Handler handler;
	private Thread thKroke;
	private String[] arraysSpliter;

	public AnimHh() {
		X_SCALE = 0;
		Y_SCALE = 0;
		X_TRANS = 0;
		Y_TRANS = 0;
		MARGE = 1.0f;
	}

	public void startZoomIn(final ImageView image, int fromX, int fromY) {
		X_SCALE += fromX;
		Y_SCALE += fromY;
		handler = new Handler();
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				for (int i = 0; i <= 100; i++) {
					try {
						Thread.sleep(50);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					handler.post(new Runnable() {
						@Override
						public void run() {
							MARGE += 0.005f;
							Matrix mtrx = new Matrix();
							mtrx.postScale(MARGE, MARGE);
							mtrx.postTranslate(X_SCALE, Y_SCALE);
							image.setImageMatrix(mtrx);
							image.setScaleType(ScaleType.MATRIX);
							image.invalidate();
							X_SCALE -= 1;
							Y_SCALE -= 1;
						}
					});
				}
			}
		};
		new Thread(runnable).start();
	}

	public Point getCurrentZoom() {
		return new Point(X_SCALE, Y_SCALE);
	}

	public void startZoomOut(final ImageView image, int fromX, int fromY) {
		X_SCALE = fromX;
		Y_SCALE = fromY;
		handler = new Handler();
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				for (int i = 0; i <= 100; i++) {
					try {
						Thread.sleep(50);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					handler.post(new Runnable() {
						@Override
						public void run() {
							MARGE -= 0.005f;
							Matrix mtrx = new Matrix();
							mtrx.postScale(MARGE, MARGE);
							mtrx.postTranslate(X_SCALE, Y_SCALE);
							image.setImageMatrix(mtrx);
							image.setScaleType(ScaleType.MATRIX);
							image.invalidate();
							X_SCALE += 1;
							Y_SCALE += 1;
						}
					});
				}
			}
		};
		new Thread(runnable).start();
	}

	public void startScalHoriz(final ImageView image, int TrfromX, int TrfromY,
			final float margin) {
		X_TRANS = TrfromX;
		Y_TRANS = TrfromY;
		handler = new Handler();
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				for (int i = 0; i <= 300; i++) {
					try {
						Thread.sleep(5);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					handler.post(new Runnable() {
						@Override
						public void run() {
							Matrix mtrx = new Matrix();
							mtrx.postScale(margin, margin);
							mtrx.postTranslate(X_TRANS, Y_TRANS);
							image.setImageMatrix(mtrx);
							image.setScaleType(ScaleType.MATRIX);
							image.invalidate();
							X_TRANS -= 1;

						}
					});
				}
			}
		};
		new Thread(runnable).start();
	}

	public void startKaroke(final Activity activity, final TextView tv,
			final MediaPlayer mp, final String txt) {
		handler = new Handler();
		counter = 0;
		arraysSpliter = new String[txt.length()];
		arraysSpliter = txt.split(" ");
		final int time = mp.getDuration() / arraysSpliter.length;
		System.out.println("---------------------------------------------------");
		System.out.println("********************** TXT TAILLE :"+ txt.length());
		System.out.println("********************** TAILLE :"+ arraysSpliter.length);
		System.out.println("********************** THREAD STATUS :"+ StopedThread);
		System.out.println("********************** DURATION :" + time);
		final Runnable runnable = new Runnable() {
			@Override
			public void run() {
				while (!StopedThread && counter < arraysSpliter.length - 1) {
					try {
						Thread.sleep(time);
						handler.post(new Runnable() {
							@Override
							public void run() {
								tv.append(arraysSpliter[counter] + " ");
								arraysSpliter[counter] = "";
								System.out.println("********************** TAILLE :"+ arraysSpliter.length);
								System.out.println("COUNTER : " + counter);
							}
						});
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					counter++;

				}

			}
		};

		thKroke = new Thread(runnable);
		thKroke.start();
	}
}