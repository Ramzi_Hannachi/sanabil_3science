package com.sanabilmed.science3N.M1.m2;

import java.util.Random;
import java.util.Vector;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.os.PowerManager.WakeLock;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sanabilmed.science3N.Act_Modules;
import com.sanabilmed.science3N.R;
import com.sanabilmed.science3N.M1.m1.DragWithFleche;
import com.sanabilmed.science3N.M1.m1.Ex2_m1;
import com.sanabilmed.science3N.M1.m1.Ex3_m1;
import com.sanabilmed.science3N.M1.m1.Image;
import com.sanabilmed.science3N.M1.m1.Page1_m1;

public class Ex1_m2 extends Activity {

	ImageView obj_1, obj_2, obj_3, obj_4, obj_5;
	LinearLayout za1, za2, za3, za4, za5;
	LinearLayout zd1, zd2, zd3, zd4, zd5;
	private WakeLock mWakeLock;
	private ImageView rep, ex3, ex2, ex4, ex5, ex6, ex7, ex8, ex9, audio,
			imgVer, imgCorr, imgVerEx2, img1, img2, img3, img4, exit, home;
	private MediaPlayer mp;
	private ImageView rep1, rep2, rep3, rep4, rep5, rep6, rep7, rep8;

	AnimationSet animation;
	boolean _rep1 = false, _rep2 = false, _rep3 = false, _rep4 = false,
			_rep5 = false, _rep6 = false, _rep7 = false, _rep8 = false;
	private boolean scalingComplete = false;
	Activity act;
	ClipData data;
	int randomInt;

	private static final long active = 2000;
	private static final int STOP = 0;
	private Message msg;

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public void onCreate(Bundle savedInstanceState) {

		setContentView(R.layout.ex1_m2);
		exit = (ImageView) findViewById(R.id.exit);
		home = (ImageView) findViewById(R.id.home);

		act = this;
		rep = (ImageView) findViewById(R.id.repeat);
		rep1 = (ImageView) findViewById(R.id.rep1);
		rep2 = (ImageView) findViewById(R.id.rep2);
		rep3 = (ImageView) findViewById(R.id.rep3);
		rep4 = (ImageView) findViewById(R.id.rep4);
		rep5 = (ImageView) findViewById(R.id.rep5);
		rep6 = (ImageView) findViewById(R.id.rep6);
		rep7 = (ImageView) findViewById(R.id.rep7);
		rep8 = (ImageView) findViewById(R.id.rep8);

		img2 = (ImageView) findViewById(R.id.b2);

		rep1.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				if (_rep1 == false) {
					_rep1 = true;
					rep1.setBackgroundResource(R.drawable.img1_s_m2_ex1);
				} else {
					_rep1 = false;
					rep1.setBackgroundResource(R.drawable.img1_m2_ex1);
				}
			}
		});

		rep2.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				if (_rep2 == false) {
					_rep2 = true;
					rep2.setBackgroundResource(R.drawable.img2_s_m2_ex1);
				} else {
					_rep2 = false;
					rep2.setBackgroundResource(R.drawable.img2_m2_ex1);
				}
			}
		});

		rep3.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				if (_rep3 == false) {
					_rep3 = true;
					rep3.setBackgroundResource(R.drawable.img3_s_m2_ex1);
				} else {
					_rep3 = false;
					rep3.setBackgroundResource(R.drawable.img3_m2_ex1);
				}
			}
		});
		rep4.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				if (_rep4 == false) {
					_rep4 = true;
					rep4.setBackgroundResource(R.drawable.img4_s_m2_ex1);
				} else {
					_rep4 = false;
					rep4.setBackgroundResource(R.drawable.img4_m2_ex1);
				}
			}
		});

		rep5.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				if (_rep5 == false) {
					_rep5 = true;
					rep5.setBackgroundResource(R.drawable.img5_s_m2_ex1);
				} else {
					_rep5 = false;
					rep5.setBackgroundResource(R.drawable.img5_m2_ex1);
				}
			}
		});

		rep6.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				if (_rep6 == false) {
					_rep6 = true;
					rep6.setBackgroundResource(R.drawable.img6_s_m2_ex1);
				} else {
					_rep6 = false;
					rep6.setBackgroundResource(R.drawable.img6_m2_ex1);
				}
			}
		});

		rep7.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				if (_rep7 == false) {
					_rep7 = true;
					rep7.setBackgroundResource(R.drawable.img7_s_m2_ex1);
				} else {
					_rep7 = false;
					rep7.setBackgroundResource(R.drawable.img7_m2_ex1);
				}
			}
		});
		rep8.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				if (_rep8 == false) {
					_rep8 = true;
					rep8.setBackgroundResource(R.drawable.img8_s_m2_ex1);
				} else {
					_rep8 = false;
					rep8.setBackgroundResource(R.drawable.img8_m2_ex1);
				}
			}
		});

		ex3 = (ImageView) findViewById(R.id.ex_3);
		ex2 = (ImageView) findViewById(R.id.ex_2);
		ex4 = (ImageView) findViewById(R.id.ex_4);
		ex5 = (ImageView) findViewById(R.id.ex_5);
		ex6 = (ImageView) findViewById(R.id.ex_6);
		ex7 = (ImageView) findViewById(R.id.ex_7);
		ex8 = (ImageView) findViewById(R.id.ex_8);
		ex9 = (ImageView) findViewById(R.id.ex_9);
		imgCorr = (ImageView) findViewById(R.id.imgCorr);
		imgVer = (ImageView) findViewById(R.id.imgVer);
		rep = (ImageView) findViewById(R.id.repeat);
		audio = (ImageView) findViewById(R.id.jouerSon);

		// ////////////////////////////fin gestion
		// mp////////////////////////////////

		Animation fadeIn = new AlphaAnimation(0, 1);
		fadeIn.setInterpolator(new DecelerateInterpolator()); // add this
		fadeIn.setDuration(1000);

		Animation fadeOut = new AlphaAnimation(1, 0);
		fadeOut.setInterpolator(new AccelerateInterpolator()); // and this
		fadeOut.setStartOffset(2500);// 3500
		fadeOut.setDuration(1000);

		animation = new AnimationSet(true); // change to false
		animation.addAnimation(fadeIn);
		animation.addAnimation(fadeOut);

		super.onCreate(savedInstanceState);

		imgVer.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Random rand = new Random();
				randomInt = rand.nextInt();
				imgCorr.setBackgroundDrawable(null);
				imgCorr.setBackgroundDrawable(null);
				imgCorr.setBackgroundDrawable(null);
				try_stop_mp(mp);

				if (_rep1 == true) {
					rep1.setBackgroundResource(R.drawable.img1_f_m2_ex1);
				}

				if (_rep2 == true) {
					rep2.setBackgroundResource(R.drawable.img2_v_m2_ex1);
				}

				if (_rep3 == true) {
					rep3.setBackgroundResource(R.drawable.img3_f_m2_ex1);
				}

				if (_rep4 == true) {
					rep4.setBackgroundResource(R.drawable.img4_v_m2_ex1);
				}
				if (_rep5 == true) {
					rep5.setBackgroundResource(R.drawable.img5_v_m2_ex1);
				}
				if (_rep6 == true) {
					rep6.setBackgroundResource(R.drawable.img6_f_m2_ex1);
				}
				if (_rep7 == true) {
					rep7.setBackgroundResource(R.drawable.img7_v_m2_ex1);
				}
				if (_rep8 == true) {
					rep8.setBackgroundResource(R.drawable.img8_v_m2_ex1);
				}

				if ((_rep1 == false && _rep3 == false && _rep6 == false)
						&& (_rep2 == false || _rep4 == false || _rep5 == false
								|| _rep7 == false || _rep8 == false)) {
					imgCorr.setVisibility(1);
					imgCorr.setBackgroundResource(R.drawable.mascotte_incomplet);
					imgCorr.startAnimation(animation);
					imgCorr.setVisibility(View.INVISIBLE);
					mp = MediaPlayer.create(getApplicationContext(),
							R.raw.mascotte_incomplet);
					mp.start();

				}

				else if (_rep2 == true && _rep4 == true && _rep5 == true
						&& _rep7 == true && _rep8 == true) {
					imgCorr.setVisibility(1);
					imgCorr.setBackgroundResource(R.drawable.mascotte_vrai_1);
					imgCorr.startAnimation(animation);
					imgCorr.setVisibility(View.INVISIBLE);
					mp = MediaPlayer.create(getApplicationContext(),
							R.raw.mascotte_vrai_1);
					mp.start();

					rep1.setEnabled(false);
					rep2.setEnabled(false);
					rep3.setEnabled(false);
					rep4.setEnabled(false);

				} else if (_rep1 == true && _rep3 == true && _rep4 == true
						&& _rep2 == true && _rep5 == true && _rep6 == true
						&& _rep7 == true && _rep8 == true) {
					imgCorr.setVisibility(1);
					imgCorr.setBackgroundResource(R.drawable.mascotte_faux_2);
					imgCorr.startAnimation(animation);
					imgCorr.setVisibility(View.INVISIBLE);

					mp = MediaPlayer.create(getApplicationContext(),
							R.raw.mascotte_faux_2);
					mp.start();
					mp.setOnCompletionListener(new OnCompletionListener() {

						@Override
						public void onCompletion(MediaPlayer mp) {
							_rep1 = false;
							_rep2 = false;
							_rep3 = false;
							_rep4 = false;
							_rep5 = false;
							_rep6 = false;
							_rep7 = false;
							_rep8 = false;
							rep1.setBackgroundResource(R.drawable.img1_m2_ex1);
							rep2.setBackgroundResource(R.drawable.img2_m2_ex1);
							rep3.setBackgroundResource(R.drawable.img3_m2_ex1);
							rep4.setBackgroundResource(R.drawable.img4_m2_ex1);
							rep5.setBackgroundResource(R.drawable.img5_m2_ex1);
							rep6.setBackgroundResource(R.drawable.img6_m2_ex1);
							rep7.setBackgroundResource(R.drawable.img7_m2_ex1);
							rep8.setBackgroundResource(R.drawable.img8_m2_ex1);
						}
					});
				} else {
					imgCorr.setVisibility(1);
					imgCorr.setBackgroundResource(R.drawable.mascotte_faux_2);
					imgCorr.startAnimation(animation);
					imgCorr.setVisibility(View.INVISIBLE);

					mp = MediaPlayer.create(getApplicationContext(),
							R.raw.mascotte_faux_2);
					mp.start();
					mp.setOnCompletionListener(new OnCompletionListener() {

						@Override
						public void onCompletion(MediaPlayer mp) {
							_rep1 = false;
							_rep3 = false;
							_rep6 = false;
							rep1.setBackgroundResource(R.drawable.img1_m2_ex1);
							rep3.setBackgroundResource(R.drawable.img3_m2_ex1);
							rep6.setBackgroundResource(R.drawable.img6_m2_ex1);
						}
					});
				}

				/*
				 * switch (verifierReponse()) { case 0:
				 * imgCorr.setVisibility(1);
				 * imgCorr.setBackgroundResource(R.drawable.mascotte_incomplet);
				 * imgCorr.startAnimation(animation);
				 * imgCorr.setVisibility(View.INVISIBLE); mp =
				 * MediaPlayer.create(getApplicationContext(),
				 * R.raw.mascotte_incomplet); mp.start(); break; case 1:
				 * imgCorr.setVisibility(1);
				 * imgCorr.setBackgroundResource(R.drawable.mascotte_faux_2);
				 * imgCorr.startAnimation(animation);
				 * imgCorr.setVisibility(View.INVISIBLE);
				 * 
				 * mp = MediaPlayer.create(getApplicationContext(),
				 * R.raw.mascotte_faux_2); mp.start(); break; case 2:
				 * imgCorr.setVisibility(1);
				 * imgCorr.setBackgroundResource(R.drawable.mascotte_vrai_1);
				 * imgCorr.startAnimation(animation);
				 * imgCorr.setVisibility(View.INVISIBLE);
				 * 
				 * mp = MediaPlayer.create(getApplicationContext(),
				 * R.raw.mascotte_vrai_1); mp.start(); break; }
				 */
			}
		});

		img2.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				try_stop_mp(mp);
				Intent i2 = new Intent(getApplicationContext(), Page1_m2.class);
				startActivity(i2);
				act.finish();
			}
		});

		home.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				try_stop_mp(mp);
				startActivity(new Intent(getApplicationContext(), Act_Modules.class));

				act.finish();

			}
		});
		exit.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				try_stop_mp(mp);
				finish();

			}
		});
		ex3.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				try_stop_mp(mp);
				startActivity(new Intent(getApplicationContext(), Ex3_m2.class));

				act.finish();

			}
		});
		ex2.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				try_stop_mp(mp);
				startActivity(new Intent(getApplicationContext(), Ex2_m2.class));

				act.finish();

			}
		});

		ex4.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				try_stop_mp(mp);
				try_stop_mp(mp);
				startActivity(new Intent(getApplicationContext(), Ex4_m2.class));

				act.finish();
			}
		});

		ex5.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				try_stop_mp(mp);
				try_stop_mp(mp);
				startActivity(new Intent(getApplicationContext(), Ex5_m2.class));

				act.finish();
			}
		});

		ex6.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				try_stop_mp(mp);
				if (isOnline()) {
					Intent intUrl = new Intent(Intent.ACTION_VIEW);
					intUrl.setData(Uri.parse("http://google.com"));
					startActivity(intUrl);
				} else {
					Toast.makeText(getApplicationContext(),
							"V�rifier Votre Connexion ", Toast.LENGTH_SHORT)
							.show();
				}
				// Intent i2 = new Intent(getApplicationContext(),
				// Tri_1_Mod_1_Ex_6.class);
				// startActivity(i2);
				// act.finish();
			}
		});

		ex7.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				try_stop_mp(mp);
				if (isOnline()) {
					Intent intUrl = new Intent(Intent.ACTION_VIEW);
					intUrl.setData(Uri.parse("http://google.com"));
					startActivity(intUrl);
				} else {
					Toast.makeText(getApplicationContext(),
							"V�rifier Votre Connexion ", Toast.LENGTH_SHORT)
							.show();
				}
				// Intent i2 = new Intent(getApplicationContext(),
				// Tri_1_Mod_1_Ex_7.class);
				// startActivity(i2);
				// act.finish();
			}
		});

		ex8.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				try_stop_mp(mp);
				if (isOnline()) {
					Intent intUrl = new Intent(Intent.ACTION_VIEW);
					intUrl.setData(Uri.parse("http://google.com"));
					startActivity(intUrl);
				} else {
					Toast.makeText(getApplicationContext(),
							"V�rifier Votre Connexion ", Toast.LENGTH_SHORT)
							.show();
				}
				// Intent i2 = new Intent(getApplicationContext(),
				// Tri_1_Mod_1_Ex_8.class);
				// startActivity(i2);
				// act.finish();
			}
		});

		ex9.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				try_stop_mp(mp);
				if (isOnline()) {
					Intent intUrl = new Intent(Intent.ACTION_VIEW);
					intUrl.setData(Uri.parse("http://google.com"));
					startActivity(intUrl);
				} else {
					Toast.makeText(getApplicationContext(),
							"V�rifier Votre Connexion ", Toast.LENGTH_SHORT)
							.show();
				}
				// Intent i2 = new Intent(getApplicationContext(),
				// Tri_1_Mod_1_Ex_9.class);
				// startActivity(i2);
				// act.finish();
			}
		});
		imgCorr.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				imgCorr.setVisibility(View.INVISIBLE);

			}
		});

		rep.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				try_stop_mp(mp);
				finish();
				startActivity(getIntent());
			}
		});

		audio.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				try_stop_mp(mp);
				mp = new MediaPlayer();
				mp = MediaPlayer.create(Ex1_m2.this, R.raw.ex1_m2);
				mp.start();
			}
		});

	}

	public void try_stop_mp(MediaPlayer mp) {
		try {

			if (mp != null) {
				mp.stop();
				mp.release();
			}

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	// protected void onDestroy() {
	// mp.release();
	// mWakeLock.release();
	// super.onDestroy();
	// }

	protected void onRestart() {
		super.onRestart();
	}

	protected void onPause() {
		act.finish();
		super.onPause();
	}

	// ///////////////////////////////////////////////////////////////////////////////////////////
	public void onWindowFocusChanged(boolean hasFocus) {
		if (!scalingComplete) // only do this once
		{
			scaleContents(findViewById(R.id.contents),
					findViewById(R.id.container));
			scalingComplete = true;
		}
		super.onWindowFocusChanged(hasFocus);
	}

	/**
	 * Called when the views have been created. We override this in order to
	 * scale the UI, which we can't do before this.
	 */
	@Override
	public View onCreateView(String name, Context context, AttributeSet attrs) {
		View view = super.onCreateView(name, context, attrs);
		return view;
	}

	private void scaleContents(View rootView, View container) {
		float xScale = (float) container.getWidth() / rootView.getWidth();
		float yScale = (float) container.getHeight() / rootView.getHeight();
		float scale = Math.min(xScale, yScale);
		scaleViewAndChildren(rootView, scale);
	}

	public static void scaleViewAndChildren(View root, float scale) {
		ViewGroup.LayoutParams layoutParams = root.getLayoutParams();

		// Scale the view itself
		if (layoutParams.width != ViewGroup.LayoutParams.FILL_PARENT
				&& layoutParams.width != ViewGroup.LayoutParams.WRAP_CONTENT) {
			layoutParams.width *= scale;
		}
		if (layoutParams.height != ViewGroup.LayoutParams.FILL_PARENT
				&& layoutParams.height != ViewGroup.LayoutParams.WRAP_CONTENT) {
			layoutParams.height *= scale;
		}

		if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
			ViewGroup.MarginLayoutParams marginParams = (ViewGroup.MarginLayoutParams) layoutParams;
			marginParams.leftMargin *= scale;
			marginParams.rightMargin *= scale;
			marginParams.topMargin *= scale;
			marginParams.bottomMargin *= scale;
		}

		root.setLayoutParams(layoutParams);

		root.setPadding((int) (root.getPaddingLeft() * scale),
				(int) (root.getPaddingTop() * scale),
				(int) (root.getPaddingRight() * scale),
				(int) (root.getPaddingBottom() * scale));

		if (root instanceof TextView) {
			TextView textView = (TextView) root;
			textView.setTextSize(textView.getTextSize() * scale);
		}

		if (root instanceof ViewGroup) {
			ViewGroup groupView = (ViewGroup) root;
			for (int cnt = 0; cnt < groupView.getChildCount(); ++cnt)
				scaleViewAndChildren(groupView.getChildAt(cnt), scale);
		}
	}

	public boolean isOnline() {

		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}
}