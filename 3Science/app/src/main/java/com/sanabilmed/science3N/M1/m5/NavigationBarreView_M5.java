package com.sanabilmed.science3N.M1.m5;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;

import com.sanabilmed.science3N.R;
import com.sanabilmed.science3N.otherClasses.Hh;
import com.sanabilmed.science3N.otherClasses.HorizontalListView;

public class NavigationBarreView_M5 extends RelativeLayout implements
OnClickListener, OnItemClickListener {

	private View mConvertView;
	private static RelativeLayout barre;
	private RelativeLayout buttonSliding;
	private ImageView imNext, imPrev, imHome;
	private static ImageView SlideButton;
	private OnDispatchClickListener mListenerClick;
	private OnItemClickListener mItemListenerClick;
	private static Context context;
	static RelativeLayout Currentview;
	private HashMap<String, String> map;
	private ArrayList<HashMap<String, String>> listItem;
	private static HorizontalListView listview;


	public interface OnDispatchClickListener {
		public void onDispatchClick(int id);
	}

	public interface OnItemClickListener {
		public void onDispatchItemClick(int position);
	}

	public NavigationBarreView_M5(final Context context, AttributeSet attrs) {

		super(context, attrs);
		NavigationBarreView_M5.context = context;
		mConvertView = LayoutInflater.from(context).inflate(
				R.layout.navigation_barre, this);

		buttonSliding = (RelativeLayout) findViewById(R.id.SlideButtonFrame);
		imNext = (ImageView) mConvertView.findViewById(R.id.imNext);
		imNext.setOnClickListener(this);
		imPrev = (ImageView) mConvertView.findViewById(R.id.imPrev);
		imPrev.setOnClickListener(this);
		imHome = (ImageView) mConvertView.findViewById(R.id.imHome);
		imHome.setOnClickListener(this);
		barre = (RelativeLayout) mConvertView.findViewById(R.id.Barre);
		SlideButton = (ImageView) mConvertView.findViewById(R.id.SlideButton);
		buttonSliding.setOnClickListener(this);
		listview = (HorizontalListView) mConvertView
				.findViewById(R.id.listview);
		listItem = new ArrayList<HashMap<String, String>>();
		upDateItem(Hh.selectedPage);
		final SimpleAdapter mSchedule = new SimpleAdapter(context, listItem,
				R.layout.affichageitems, new String[] { "img" },
				new int[] { R.id.img });

		listview.setAdapter(mSchedule);
		listview.setOnItemClickListener(this);
		Currentview = this;


	}

	public void setOnDispatchClickListener(OnDispatchClickListener v) {
		mListenerClick = v;
	}

	public void setOnDispatchItemClickListener(OnItemClickListener v) {
		mItemListenerClick = v;
	}


	@Override
	public void onClick(View arg0) {
		mListenerClick.onDispatchClick(arg0.getId());

		switch (arg0.getId()) {
		case R.id.SlideButtonFrame:
			this.startAnimation(AnimationUtils.loadAnimation(context,
					R.anim.push_down));
			this.setVisibility(View.INVISIBLE);
			break;
		default:
			break;
		}
	}

	public static void showBarre() {
		SlideButton.setBackgroundDrawable(null);
		SlideButton.setBackgroundResource(R.drawable.bas);
		barre.startAnimation(AnimationUtils.loadAnimation(context,
				R.anim.push_up));
		Currentview.setVisibility(View.VISIBLE);
	}

	public static void hideBarre() {
		Currentview.setVisibility(View.INVISIBLE);
	}

	public static void MoveToNextItem() {

		listview.moveToRight();

	}

	public static void MoveToPrevItem() {

		listview.moveToLeft();
	}

	public void upDateItem(int selectedView) {
		for (int i = 0; i < iconBarreOffs.length; i++) {
			map = new HashMap<String, String>();
			if (i == selectedView) {
				map.put("img", String.valueOf(iconBarreOns[i]));
				listItem.add(map);
			} else {
				map.put("img", String.valueOf(iconBarreOffs[i]));
				listItem.add(map);
			}
		}

	}

	@Override
	public void onItemClick(AdapterView<?> a, View v, int position, long id) {
		mItemListenerClick.onDispatchItemClick(position);
		LinearLayout item = (LinearLayout) v;
		((ImageView) item.getChildAt(0))
		.setImageResource(android.R.color.transparent);
		((ImageView) item.getChildAt(0)).setImageResource(iconBarreOns[position]);
		System.out.println("GET LAST VISIBLE POSTION :"
				+ listview.getLastVisiblePosition());
	}

	private static int[] iconBarreOffs = { R.drawable.ic_1_inactif,
		R.drawable.ic_2_inactif, R.drawable.ic_3_inactif};
	private static int[] iconBarreOns = { R.drawable.ic_1,
		R.drawable.ic_2, R.drawable.ic_3};

}
