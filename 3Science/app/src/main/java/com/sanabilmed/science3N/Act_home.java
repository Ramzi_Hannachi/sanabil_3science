package com.sanabilmed.science3N;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
public class Act_home extends Activity 
{
	private static final int STOPSPLASH = 0;
	private static final long SPLASHTIME = 2000;
	private boolean scalingComplete = false;
	private Handler splashHandler = new Handler() 
	{
		public void handleMessage(Message msg) 
		{
			Intent intent = new Intent(Act_home.this, Act_Modules.class);
			Act_home.this.startActivity(intent);
			Act_home.this.finish();
			overridePendingTransition(R.anim.mainfadein, R.anim.splashfadeout);

			super.handleMessage(msg);
		}
	};

	// Activity act;   ramzi 

	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
		//	act = this; ramzi

		Message msg = new Message();
		msg.what = STOPSPLASH;

		splashHandler.sendMessageDelayed(msg, SPLASHTIME);
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{
		if ((keyCode == KeyEvent.KEYCODE_BACK)) 
		{
			splashHandler.removeMessages(STOPSPLASH);
		}
		return super.onKeyDown(keyCode, event);
	}










	//////////////////////////////////////////////////////////////////////////////////////////////////	

	public void onWindowFocusChanged(boolean hasFocus)
	{
		if (!scalingComplete) // only do this once
		{
			scaleContents(findViewById(R.id.contents),
					findViewById(R.id.container));
			scalingComplete = true;
		}
		super.onWindowFocusChanged(hasFocus);
	}

	/**
	 * Called when the views have been created. We override this in order to
	 * scale the UI, which we can't do before this.
	 */
	@Override
	public View onCreateView(String name, Context context, AttributeSet attrs) {
		View view = super.onCreateView(name, context, attrs);
		return view;
	}

	/**
	 * Scales the contents of the given view so that it completely fills the
	 * given container on one axis (that is, we're scaling isotropically).
	 * 
	 * @param rootView
	 *            The view that contains the interface elements
	 * @param container
	 *            The view into which the interface will be scaled
	 */
	private void scaleContents(View rootView, View container) {
		// Compute the scaling ratio. Note that there are all kinds of games you
		// could
		// play here - you could, for example, allow the aspect ratio to be
		// distorted
		// by a certain percentage, or you could scale to fill the *larger*
		// dimension
		// of the container view (useful if, for example, the container view can
		// scroll).
		float xScale = (float) container.getWidth() / rootView.getWidth();
		float yScale = (float) container.getHeight() / rootView.getHeight();
		float scale = Math.min(xScale, yScale);

		// Scale our contents
		scaleViewAndChildren(rootView, scale);
	}

	/**
	 * Scale the given view, its contents, and all of its children by the given
	 * factor.
	 * 
	 * @param root
	 *            The root view of the UI subtree to be scaled
	 * @param scale
	 *            The scaling factor
	 */
	public static void scaleViewAndChildren(View root, float scale) {
		// Retrieve the view's layout information
		ViewGroup.LayoutParams layoutParams = root.getLayoutParams();

		// Scale the view itself
		if (layoutParams.width != ViewGroup.LayoutParams.FILL_PARENT
				&& layoutParams.width != ViewGroup.LayoutParams.WRAP_CONTENT) {
			layoutParams.width *= scale;
		}
		if (layoutParams.height != ViewGroup.LayoutParams.FILL_PARENT
				&& layoutParams.height != ViewGroup.LayoutParams.WRAP_CONTENT) {
			layoutParams.height *= scale;
		}

		// If this view has margins, scale those too
		if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
			ViewGroup.MarginLayoutParams marginParams = (ViewGroup.MarginLayoutParams) layoutParams;
			marginParams.leftMargin *= scale;
			marginParams.rightMargin *= scale;
			marginParams.topMargin *= scale;
			marginParams.bottomMargin *= scale;
		}

		root.setLayoutParams(layoutParams);

		root.setPadding((int) (root.getPaddingLeft() * scale),
				(int) (root.getPaddingTop() * scale),
				(int) (root.getPaddingRight() * scale),
				(int) (root.getPaddingBottom() * scale));

		if (root instanceof TextView) 
		{
			TextView textView = (TextView) root;
			textView.setTextSize(textView.getTextSize() * scale);
		}

		// If the root view is a ViewGroup, scale all of its children
		// recursively
		if (root instanceof ViewGroup)
		{
			ViewGroup groupView = (ViewGroup) root;
			for (int cnt = 0; cnt < groupView.getChildCount(); ++cnt)
				scaleViewAndChildren(groupView.getChildAt(cnt), scale);
		}
	}

}