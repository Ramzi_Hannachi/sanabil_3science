package com.sanabilmed.science3N.M1.m5;

import android.os.Bundle;

import com.sanabilmed.science3N.R;

public class Page1_m5 extends Scene_m5
{

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{

		imageDrawable = R.drawable.e3_m5_s1;
		sound = R.raw.e3_m5_s1;
		hasNext = false;
		nextActivity = null;
		zoomed = true;


		super.onCreate(savedInstanceState);
	}
}