package com.sanabilmed.science3N.M1.m3;

import android.os.Bundle;

import com.sanabilmed.science3N.R;

public class Page3_m3 extends Scene_m3{

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		imageDrawable = R.drawable.sc3_m3;
		sound = R.raw.sc3_m3;
		hasNext = false;
		nextActivity = null;
		zoomed = true;

		super.onCreate(savedInstanceState);
	}
}