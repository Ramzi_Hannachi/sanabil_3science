package com.sanabilmed.science3N.otherClasses;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.sanabilmed.science3N.R;

//import com.sanabilmed.arabe.R;

public class ImageAdapter extends ArrayAdapter<Integer> 
{
	Context mContext;
	int layoutResourceId;
	Integer data[] = null;
	public ImageAdapter(Context mContext, int layoutResourceId, Integer[] data) 
	{
		super(mContext, layoutResourceId, data);
		this.layoutResourceId = layoutResourceId;
		this.mContext = mContext;
		this.data = data;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		View listItem = convertView;

		LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
		listItem = inflater.inflate(layoutResourceId, parent, false);

		ImageView imageViewFolderIcon = (ImageView) listItem.findViewById(R.id.trim1);

		Integer folder = data[position];

		imageViewFolderIcon.setImageResource(folder);

		return listItem;
	}

}
