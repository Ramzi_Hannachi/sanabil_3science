package com.sanabilmed.science3N.M1.m1;

import android.os.Bundle;

import com.sanabilmed.science3N.R;

public class Page2_m1 extends Scene_m1
{
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		imageDrawable = R.drawable.sc2_m1;
		sound = R.raw.sc2_m1;
		hasNext = false;
		nextActivity = null;
		zoomed = true;
		super.onCreate(savedInstanceState);
	}
}